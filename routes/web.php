<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\TagTekst;
use App\Tag;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('home');
})->middleware('auth');

Auth::routes(['register' => false]);
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::resource('contacten', 'ContactController')->middleware('auth');
Route::get('/contacten/{id}/momentverwijderen', 'ContactController@moment_verwijderen')->middleware('auth');
Route::get('/contacten/{id}/meldklaar', 'ContactController@meldklaar')->middleware('auth');
Route::get('/contacten/{id}/verwijdertag', 'ContactController@verwijdertag')->middleware('auth');


Route::resource('momenten', 'MomentController')->middleware('auth');
Route::resource('evenementen', 'BijeenkomstController')->middleware('auth');
Route::resource('gebruikers', 'GebruikerController')->middleware('auth');
Route::resource('organisaties', 'OrganisatieController')->middleware('auth');
Route::resource('tags', 'TagController')->middleware('auth');
Route::post('tags/wijzig', 'TagController@wijzig')->middleware('auth');

Route::post('deelname/toevoegen', 'BijeenkomstController@deelname_toevoegen')->middleware('auth');
Route::get('deelname/verwijderen/{id}', 'BijeenkomstController@deelname_verwijderen')->middleware('auth');
Route::put('/deelname/{id}', 'DeelnameController@update')->middleware('auth');

Route::post('/evenement/toevoegen', 'ContactController@bijeenkomst_toevoegen')->middleware('auth');
Route::get('/evenement/verwijderen/{id}', 'ContactController@bijeenkomst_verwijderen')->middleware('auth');
Route::post('/evenement/exporteerEmail/{id}', 'BijeenkomstController@exporteerEmail')->middleware('auth');
