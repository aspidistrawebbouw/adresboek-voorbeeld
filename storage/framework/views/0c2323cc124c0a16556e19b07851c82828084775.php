<?php /* /home/jos/git/aspidistra/lunamareadresboek/resources/views/contacten/form.blade.php */ ?>
<?php $__env->startSection('title', 'Details contactpersoon'); ?>
<?php $__env->startSection('content'); ?>
<p>Spring naar: <a href="#cm">Contactmomenten</a> | <a href="#bk">Bijeenkomsten</a></p>
<h3>Details <?php if(isset($contact)): ?> <?php echo e($contact->voornaam); ?> <?php echo e($contact->tussenvoegsel); ?> <?php echo e($contact->achternaam); ?><?php else: ?> contactpersoon <?php endif; ?></h3>
<div id="app">
	<?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->yieldContent('content'); ?>
	<?php if($errors->any()): ?>
		<ul class="alert alert-danger">
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li ><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
	<?php endif; ?> 
</div>
<form action="<?php echo e($action); ?>" method="POST"  enctype="multipart/form-data">
<?php echo csrf_field(); ?>
<?php 
use Illuminate\Support\Facades\DB;
use App\Contact;
use App\Deelname;
use App\Bijeenkomst;
use App\Organisatie;
use App\Moment;
use App\User;
setlocale(LC_ALL, 'nl_NL.utf8');
	if (isset($contact->id) && $contact->id != 0)
	{ ?>
		<input type="hidden" name="_method" value="PUT">
<?php } else 
	{
		if (!isset($contact->id))
		{
			$contact = new contact();
		}
	} ?>
<style>fieldset label {width: 115px;} </style>
<fieldset>
	<p>Velden met * zijn verplichte velden.</p>
	<p>
		<label for="voornaam">Voornaam:</label> <input  class="focushier" type='text' id='voornaam' name='voornaam' value='<?php echo e($contact->voornaam); ?>'></input>
		<label for="tussenvoegsel">Tussenvoegsel:</label> <input type='text' id='tussenvoegsel' name='tussenvoegsel' value='<?php echo e($contact->tussenvoegsel); ?>' size="8"></input>
		<label for="achternaam">Achternaam*:</label> <input type='text' id='achternaam' name='achternaam' value='<?php echo e($contact->achternaam); ?>' required size="30"></input>
	</p>
	<p>
		<label for="functie">Functie:</label> <input type='text' id='functie' name='functie' value='<?php echo e($contact->functie); ?>' size="40"></input>
	</p>
	
	<p>
		<label for="locatie">Locatie:</label> <input type='text' id='locatie' name='locatie' value='<?php echo e($contact->locatie); ?>'  size="60"></input>
	<?php $dezeorg = Organisatie::where('naam',$contact->organisatie)->first(); ?>
	</p>
	<?php if(isset($contact->id)): ?>
		<div class="infoblokje">
			<p><strong>Info</strong></p>
			<p>Laatst gewijzigd: <?php echo e($contact->updated_at); ?> <?php if($contact->laatstgewijzigd): ?> door <?php echo e(User::find($contact->laatstgewijzigd)->naam); ?><?php endif; ?><br/>
			   In database sinds: <?php echo e($contact->created_at); ?><br/>
			   Gegevens afkomstig van: <?php echo e($contact->bron); ?></p>
		</div>
	<?php endif; ?>
	<p>
		<label for="organisatie">Organisatie:</label> <input type='text' id='organisatie' name='organisatie' value='<?php echo e($contact->organisatie); ?>'  size="60"></input>
	<?php if(isset($contact->organisatie) && isset($dezeorg->id)): ?>
		of <a href="<?php echo e(url('/organisaties/' . $dezeorg->id . '/edit')); ?>">ga naar deze organisatie</a></p>
		<?php if(isset($dezeorg->plaats) && $dezeorg->plaats != ""): ?><span style="margin-left: 115px;">Plaats: <?php echo e($dezeorg->plaats); ?></span><br/><?php endif; ?>
		<?php if(isset($dezeorg->email) && $dezeorg->email != ""): ?><span style="margin-left: 115px;">Email: <a href="mailto:<?php echo e($dezeorg->email); ?>"><?php echo e($dezeorg->email); ?></a></span><br/><?php endif; ?>
		<?php if(isset($dezeorg->telnr) && $dezeorg->telnr != ""): ?><span style="margin-left: 115px;">Tel.: <?php echo e($dezeorg->telnr); ?></span><br/><?php endif; ?>
		<?php if(isset($dezeorg->website) && $dezeorg->website != ""): ?><span style="margin-left: 115px;">Website: <a href="http://<?php echo e($dezeorg->website); ?>" target="_blank"><?php echo e($dezeorg->website); ?></a></span><br/><?php endif; ?>
	<?php endif; ?>
	</p>
	<p>
		<label for="email">Email-adres:</label> <input type='email' id='email' name='email' value='<?php echo e($contact->email); ?>' size="50"></input>
	</p>
	<p>
		<label for="telnr">Telnr.:</label> <input type='tel' id='telnr' name='telnr' value='<?php echo e($contact->telnr); ?>'></input>
	</p>
	<p>
		<label for="opmerkingen">Opmerkingen:</label> <textarea rows="8" cols="80" id='opmerkingen' name='opmerkingen'><?php echo e($contact->opmerkingen); ?></textarea>
	</p>
	<p>
		<label for="herkomst">Herkomst:</label>
			<select id="herkomst" name="herkomst">
				<option value="" disabled <?php if(!isset($contact->herkomst)): ?> selected <?php endif; ?>>Kies:</option>
				<?php $herkomsten = DB::table('la_contacten')->select('herkomst')->whereNotNull('herkomst')->distinct()->orderBy('herkomst')->get(); ?>
					<option value="(geen)"<?php if(!$contact->herkomst): ?> selected <?php endif; ?>>(geen)</option>
				<?php $__currentLoopData = $herkomsten; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $herkomst): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option value="<?php echo e($herkomst->herkomst); ?>" <?php if($contact->herkomst == $herkomst->herkomst): ?> selected <?php endif; ?>><?php echo e($herkomst->herkomst); ?></option>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
		&nbsp;&nbsp;<label for="herkomstanders">anders, nl:</label>
		<input id="herkomstanders" type="text" size="50" name="herkomstanders" value=""></input>
	</p>
	<p>
		<label for="geboortedatum">Geboortedatum:</label>
		<input id="geboortedatum" type="date" name="geboortedatum" value="<?php echo e($contact->geboortedatum); ?>"></input>
	</p>
	<p>
		<label for="datum_bestelling">Datum bestelling:</label>
		<input id="datum_bestelling" type="date" name="datum_bestelling" value="<?php echo e($contact->datum_bestelling); ?>"></input>
	</p>
	<p><label for="mytags">Tags:</label><span id="mytags">
	<?php if(is_a($contact, 'App\Contact')): ?>
		<?php $__currentLoopData = $contact->tags(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<span class="tag" style="background-color: <?php echo e($tag->kleur()); ?>;"><?php echo e($tag->tagtekst()); ?>&nbsp;
				<span class="tagbutton" url="<?php echo e(url('contacten/' . $tag->id . '/verwijdertag')); ?>">&#x274E;</span>
			</span>&nbsp;
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	<?php endif; ?>
	</span></p>
	<p>Bestaande tag toevoegen:&nbsp;&nbsp;
		<select name="nieuweBestaandeTag">
			<option selected disabled>Kies:</option>
		<?php
			$gebruikteTags = array();
			if (is_a($contact,'App\Contact'))
				foreach($contact->tags() as $gebruikteTag)
					$gebruikteTags[] = $gebruikteTag->tag_id;
			foreach (DB::table('la_tagteksten')->orderBy('tekst')->get() as $aot)
			{ if (!in_array($aot->id,$gebruikteTags)) 
				{ ?>
				<option value="<?php echo e($aot->id); ?>"><?php echo e($aot->tekst); ?></option>
	<?php 		}
			} ?>	
		</select>
	</p>
	<p>Of maak een nieuwe tag aan:&nbsp;&nbsp; <input name="nieuweTag" size="30"></input></p>
<button class="btn btn-primary waarsch">Niet gewijzigd</button>

</fieldset>
</form>
<?php if(is_a($contact,'App\Contact')): ?>
<?php if(isset($contact->id)): ?>
<form id="verwijderen" method="POST" action = "<?php echo e(url('contacten/' . $contact->id)); ?>">
	<?php echo csrf_field(); ?>
	<?php echo method_field('DELETE'); ?>
	<button class="onzichtbaar btn"
		teverwijderen="<?php echo e($contact->voornaam); ?> <?php echo e($contact->tussenvoegsel); ?> <?php echo e($contact->achternaam); ?>"
		><?php echo e($contact->voornaam); ?> <?php echo e($contact->tussenvoegsel); ?> <?php echo e($contact->achternaam); ?> verwijderen</button>
</form>
<br/>
<?php endif; ?>

<p><a href="<?php echo e(url('contacten')); ?>"><button class='btn btn-normaal'>&larr;Naar contacten</button></a>&nbsp;<a href="<?php echo e(url('contacten/create')); ?>"><button class='btn btn-normaal'>Nieuwe contactpersoon</button></a></p>
<?php if(isset($contact->id)): ?>
<h3 id="cm">Contact met <?php echo e($contact->voornaam); ?> <?php echo e($contact->tussenvoegsel); ?> <?php echo e($contact->achternaam); ?></h3>
<?php $momenten = Moment::where('contact_id', $contact->id)->orderBy('datum', 'desc')->get(); ?>
<fieldset>
	<form action="<?php echo e(url('/momenten')); ?>" method="POST">
	<?php echo csrf_field(); ?>
	<p><strong>Nieuw contactmoment</strong></p>
    <div class="form-group">
		<label for="datum">Datum</label>
        <input id="datum" type="date" name="datum" value="<?php echo e(date('Y-m-d')); ?>"/>
        <br/>
		<label for="tekst" style="vertical-align: top;">Tekst</label>
        <textarea id="tekst" name="tekst" rows="5" cols="80"></textarea>
        <input type="hidden" name="contact_id" value="<?php echo e($contact->id); ?>" />
        <br/>
		<label for="type">Soort contact:</label>
			<select id="type" name="type">
				<option value="" disabled selected>Kies:</option>
				<?php $types = DB::table('la_contacttypes')->orderBy('id')->get();
				foreach ($types as $type) 
					{ ?>
					<option value="<?php echo e($type->id); ?>"><?php echo e($type->type); ?></option>
				<?php } ?>
			</select>
		<label for="typeanders">&nbsp;anders, nl:</label>
		<input id="typeanders" type="text" size="50" name="typeanders"></input>
        <br/>
        <ul id="doorschuiven">
			<li>Doorschuiven naar:</li>
			<li><label for="morgen" style="white-space: nowrap;">morgen&nbsp;&nbsp;</label><input type="radio" name="periode" value="1"></input></li>
			<li><label for="volgendeweek" style="white-space: nowrap;">volgende week&nbsp;&nbsp;</label><input type="radio" name="periode" value="2"></input></li>
			<li><label for="volgendemaand" style="white-space: nowrap;;">volgende maand&nbsp;&nbsp;</label><input type="radio" name="periode" value="3"></input></li>
			<li><label for="doorschuif" style="white-space: nowrap;">specifieke datum:&nbsp;&nbsp;</label>
			<input type="date" name="doorschuif" id="doorschuif" value="<?php echo e(date('Y-m-d')); ?>"></input>
		</ul>
	</div>
	<button class="btn btn-primary waarsch" style="margin-bottom: 10px;">Niet gewijzigd</button>
	</form>
	<hr style="clear: both;">
	<?php if($momenten->count() > 0): ?>
	<p><strong>Eerdere contactmomenten</strong></p>
		<table id="momenten">
			<th>Datum</th>
			<th>Van</th>
			<th>Via</th>
			<th width="400px">Contact</th>
			<th>Geagendeerd voor</th>
			<th style="text-align: center;">Klaar</th>
			<th style="text-align: center;">Wijzig</th>
			<th style="text-align: center;">Verwijder</th>
		<?php $__currentLoopData = $momenten; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $moment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr class="toonmoment" id="toon<?php echo e($moment->id); ?>">
				<td><?php echo e($moment->datum); ?></td>
				<td><?php echo e(User::find($moment->gebruiker_id)->naam); ?></td>
				<td><?php echo e(DB::table('la_contacttypes')->where('id', $moment->type)->value('type')); ?></td>
				<td class="tekst" width="400px"><?= str_replace("\n", "<br/>", $moment->tekst) ?></td>
				<td>
					<?php if(isset($moment->doorschuif)): ?>
						<?php if($moment->periode == 1): ?> <?php echo e(strftime('%e %B', strtotime($moment->doorschuif))); ?>

						<?php elseif($moment->periode == 2): ?> de week van <?php echo e(strftime('%e %B', strtotime($moment->doorschuif))); ?>

						<?php elseif($moment->periode == 3): ?> <?php echo e(strftime('%B', strtotime($moment->doorschuif))); ?>

						<?php endif; ?>
					<?php endif; ?>
				</td>
				<td><?php if($moment->klaar): ?> <span class="okeevinkje">&#x2714;</span> 
					<?php elseif(isset($moment->doorschuif)): ?> 
						<button class="klaarmelding btn btn-normaal" 
							url="<?php echo e(url('contacten/' . $moment->id . '/meldklaar')); ?>"
							moment_id="<?php echo e($moment->id); ?>"><span class="okeevinkje">&#x2714;</span>Klaar</button><?php endif; ?></td>
				<td style="text-align: center;"><input type="image" class="toonwijzig" item="<?php echo e($moment->id); ?>" src="<?php echo e(asset('grafisch/wijzig.png')); ?>" width="15px" /></td>
				<td style="text-align: center;"><a href="<?php echo e(URL::to('contacten/' . $moment->id . '/momentverwijderen')); ?>"><input type="image" src="<?php echo e(asset('grafisch/delete.png')); ?>" width="15px" /></a></td>
			</tr>
			<tr>
				<td colspan="8">
				<div id="wijzig<?php echo e($moment->id); ?>"  class="wijzigmoment">
				<p><strong>Wijzig contactmoment:</strong></p>
				<form action="<?php echo e(url('momenten/' . $moment->id )); ?>" method="POST">
					<input type="hidden" name="contact_id" value="<?php echo e($contact->id); ?>"></input>
					<?php echo csrf_field(); ?>
					<input type="hidden" name="_method" value="PUT">
					<div style="margin: 0 20px 5px 0;">
						Datum: <input type="date" name="datum" value="<?php echo e($moment->datum); ?>"></input>
					</div>
					<textarea id="tekst" name="tekst" rows="5" cols="80"><?php echo e($moment->tekst); ?></textarea>
					<p>Soort contact:&nbsp;<select id="type" name="type">
						<?php
							$types = DB::table('la_contacttypes')->get();
							foreach ($types as $type) 
							{ ?>
								<option value="<?php echo e($type->id); ?>" <?php if($type->id == $moment->type_id): ?> selected <?php endif; ?>><?php echo e($type->type); ?></option>
						<?php } ?>
						</select>
					</p>
					<?php if(isset($moment->doorschuif)): ?>
						<p>
						Staat op de agenda voor:
						<?php if($moment->periode == 1): ?> <?php echo e(strftime('%e %B', strtotime($moment->doorschuif))); ?>

						<?php elseif($moment->periode == 2): ?> de week van <?php echo e(strftime('%e %B', strtotime($moment->doorschuif))); ?>

						<?php elseif($moment->periode == 3): ?> <?php echo e(strftime('%B', strtotime($moment->doorschuif))); ?>

						<?php endif; ?>
						en is <span class="okeevinkje">&#x2714;</span>klaar: <input type="checkbox" name="klaar" <?php if($moment->klaar): ?> checked <?php endif; ?>></input>
						</p>
					<?php endif; ?>
					<ul id="doorschuiven">
						<li>Doorschuiven naar:</li>
						<li><label for="morgen" style="white-space: nowrap;">morgen&nbsp;&nbsp;</label><input type="radio" name="periode" value="1"></input></li>
						<li><label for="volgendeweek" style="white-space: nowrap;">volgende week&nbsp;&nbsp;</label><input type="radio" name="periode" value="2"></input></li>
						<li><label for="volgendemaand" style="white-space: nowrap;;">volgende maand&nbsp;&nbsp;</label><input type="radio" name="periode" value="3"></input></li>
						<li><label for="doorschuif" style="white-space: nowrap;">specifieke datum:&nbsp;&nbsp;</label>
						<input type="date" name="doorschuif" id="doorschuif" value="<?php echo e(date('Y-m-d')); ?>"></input>
					</ul><br/>
					
					<button class="btn btn-primary waarsch" type="submit" style="float: right;" >Opslaan</button>
					</form>
					<button class="btn btn-normaal cancel" style="float: left;" item="<?php echo e($moment->id); ?>">Annuleren</button>
					</div>
				</td>
			</tr>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</table>
	<?php endif; ?>
</fieldset>
<h3 id="bk">Deelname aan bijeenkomsten</h3>
	<?php $deelname = Deelname::where('contact_id', $contact->id)->orderBy('datum', 'desc')->get(); ?>
<fieldset>
	<?php if($deelname->count() > 0): ?>
	<table>
		<?php $__currentLoopData = $deelname; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deelnam): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr><td style="padding-right: 20px;"><?php echo e($deelnam->datum); ?></td>
			<td style="padding-right: 20px;"><a href="<?php echo e(url('evenementen/' . $deelnam->bijeenkomst_id . '/edit')); ?>"><?php echo e($deelnam->bijeenkomst()); ?></a></td>
			<td style="padding-right: 20px;"><?php echo e($deelnam->type()); ?></td>
			<td style="padding-right: 20px;"><?php echo e($deelnam->opmerkingen); ?></td>
			<td style="padding-right: 20px;"><a href="<?php echo e(url('evenement/verwijderen/' . $deelnam->id)); ?>"><img src="<?php echo e(url('grafisch/delete.png')); ?>" width="15px"></a></td>
		</tr>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</table>
	<?php else: ?>
	<p><?php echo e($contact->helenaam()); ?> heeft niet aan een bijeenkomst deelgenomen.</p>
	<?php endif; ?>
	
	<p><strong><?php echo e($contact->helenaam()); ?> toevoegen aan een bijeenkomst:</strong></p>
	<form action="<?php echo e(url('/evenement/toevoegen')); ?>" method="POST">
	<?php echo csrf_field(); ?>
		<input type="hidden" name="contact_id" value="<?php echo e($contact->id); ?>"></input>
		<label for="bijeenkomst" style="width: auto; vertical-align: baseline;">Bijeenkomst:</label>
		<select name="bijeenkomst_id" id="bijeenkomst">
			<option disabled selected>Kies:</option>
		<?php
			$bijeenkomsten = Bijeenkomst::orderBy('tijdstip')->orderBy('naam')->get();
			foreach ($bijeenkomsten as $bij1k)
			{ ?>
				<option value="<?php echo e($bij1k->id); ?>"><?php echo e(strftime('%e %B %Y', strtotime($bij1k->tijdstip))); ?> - <?php echo e($bij1k->naam); ?></option>
			<?php } ?>
		</select>			
		<label for="type_id" style="width: auto; vertical-align: baseline;">Wat:</label>
		<select name="type_id" id="type_id">
		<?php
			$types = DB::table('la_deelnametypes')->get();
			foreach ($types as $type)
			{ ?>
				<option value="<?php echo e($type->id); ?>" <?php if(isset($vorigeTypeId) && $vorigeTypeId == $type->id): ?>) selected <?php endif; ?>><?php echo e($type->type); ?></option>
			<?php } ?>
		</select>
		<label for="datum" style="width: auto; vertical-align: baseline;">Datum:</label>
		<input name="datum" type="date" id="datum" value="<?php echo e(date('Y-m-d')); ?>"></input>
		<label for="opmerkingen" style="width: auto; vertical-align: baseline;">Opmerking:</label>
		<input name="opmerkingen" type="text" id="opmerkingen">
		<button class="btn btn-primary waarsch" type="submit">Niet gewijzigd</button>
	</form>
</fieldset>
	
<p><a href="<?php echo e(url('contacten')); ?>"><button class='btn btn-normaal'>&larr;Naar contacten</button></a></p>
<?php endif; ?>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
	<script src="<?php echo e(url('/js/app.js')); ?>"></script>
	<script src="<?php echo e(url('/js/js.cookie.js')); ?>"></script>
	<!-- script src="<?php echo e(url('/js/jquery.min.js')); ?>"></script -->
	<script src="<?php echo e(url('/js/ajaxfuncties.js')); ?>"></script>
	<script src="<?php echo e(url('/js/datatables.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>