<?php /* /home/jos/git/aspidistra/lunamareadresboek/resources/views/layouts/app.blade.php */ ?>
<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo e(url('favicon.ico')); ?>" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A300%2C400%2C700%2C300italic%2C400italic%2C700italic%7CBitter%3A400%2C700&subset=latin%2Clatin-ext" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('css/lunamare.css')); ?>" rel="stylesheet">
	<!-- link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" -->
	<link rel="stylesheet" href="<?php echo e(url('/css/datatables.min.css')); ?>">
	
     <title>Lunamare | <?php echo $__env->yieldContent('title'); ?></title>



</head>
<body>
	<nav class="navbar navbar-expand-md navbar-light navbar-laravel" style="position: fixed; z-index: 151; width: 1280px;">
		<div class="container">
			<a class="navbar-brand" href="<?php echo e(url('/')); ?>">
				<?php echo e(config('app.name', 'Laravel')); ?>

			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="<?php echo e(__('Toggle navigation')); ?>">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<!-- Left Side Of Navbar -->
				<ul class="navbar-nav mr-auto">
					<li><a href="<?php echo e(url('home')); ?>"><button class="btn">Home</button></a>&nbsp;</li>
					<li><a href="<?php echo e(url('contacten')); ?>"><button class="btn">Contacten</button></a>&nbsp;	</li>
					<li><a href="<?php echo e(url('evenementen')); ?>"><button class="btn">Bijeenkomsten</button></a>&nbsp;	</li>
					<li><a href="<?php echo e(url('organisaties')); ?>"><button class="btn">Organisaties</button></a>&nbsp;	</li>
					<li><a href="<?php echo e(url('tags')); ?>"><button class="btn">Tags</button></a>&nbsp;	</li>
					<li><a href="<?php echo e(url('gebruikers')); ?>"><button class="btn">Gebruikers</button></a>&nbsp;	</li>

				</ul>

				<!-- Right Side Of Navbar -->
				<ul class="navbar-nav ml-auto">
					<!-- Authentication Links -->
					<?php if(auth()->guard()->guest()): ?>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
						</li>
					<?php else: ?>
						<li class="nav-item dropdown">
							<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
								Welkom, <?php echo e(Auth::user()->naam); ?> <span class="caret"></span>
							</a>

							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
								   onclick="event.preventDefault();
												 document.getElementById('logout-form').submit();">
									<?php echo e(__('Logout')); ?>

								</a>

								<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
									<?php echo csrf_field(); ?>
								</form>
							</div>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container" style="padding-top: 100px;">
		<?php echo $__env->yieldContent('content'); ?>
	</div>
	<?php echo $__env->yieldSection(); ?>
<div class="spacer">&nbsp;</div>
<footer>
	<p style="margin-top: 20px;"><a href="https://aspidistra.nl" target="_blank">Aspidistra Webbouw</a></p>
</footer>
<?php echo $__env->yieldContent('scripts'); ?>
</body>
</html>
