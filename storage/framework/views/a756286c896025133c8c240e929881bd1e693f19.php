<?php /* /home/jos/git/aspidistra/lunamareadresboek/resources/views/home.blade.php */ ?>
<?php $__env->startSection('title', 'Home'); ?>

<?php
use App\Contact;
use App\User;
use App\Moment;
use App\Bijeenkomst;
use Illuminate\Support\Facades\DB;
?>

<?php $__env->startSection('content'); ?>
<h2>Agenda</h2>
<table id="agenda" style="width: 960px;">

<?php
	$alletijdvakken = [
		['Today', 1, 'Vandaag'],
		['Tomorrow' , 1 , 'Morgen'],
		['Monday this week' , 2 , 'Deze week'],
		['Monday next week' , 2 , 'Volgende week'],
		['first day of this month' , 3 , 'Deze maand'],
		['first day of next month' , 3 , 'Volgende maand']
	] ;
	$welitems = false;
	foreach($alletijdvakken as $mijntijdvak)
	{
		$target = new DateTime(); $target->modify($mijntijdvak[0]);
		$tedoen = Moment::where('doorschuif',$target->format('Y-m-d'))->where('periode',$mijntijdvak[1])->get();
		if ($tedoen->isNotEmpty())
		{
			$welitems = true;
			echo '<tr><td colspan="2" class="agendakop">' . $mijntijdvak[2] . '</td></tr>';

			foreach ($tedoen as $ted)
			{ 
				$contact = Contact::find($ted->contact_id);
				?>
				<tr>
					<td style="width: 750px"><a href="<?php echo e(url('contacten/' . $contact->id . '/edit#cm')); ?>"><?php echo e($contact->voornaam); ?> <?php echo e($contact->tussenvoegsel); ?> <?php echo e($contact->achternaam); ?></a><br/>
					<?php echo e($ted->tekst); ?></td>
					<td><?php if($ted->klaar): ?> <span class="okeevinkje">&#x2714;</span> 
						<?php else: ?><button class="klaarmelding btn btn-normaal" 
							url="<?php echo e(url('contacten/' . $ted->id . '/meldklaar')); ?>"
							moment_id="<?php echo e($ted->id); ?>"><span class="okeevinkje">&#x2714;</span>Klaar</button><?php endif; ?></td>
				</tr>
	<?php 	} 
		}
	}
	if (!$welitems)
	{ ?>
		<tr><td colspan="2">Geen items!</td></tr>
	<?php
	}
	?>
</table>
<hr style="margin-top: 20px; border-bottom: 1px solid orange;"/>
<h2>Logboek</h2>
<table id="logboek">
	<tr>
		<th>Naam</th>
		<th>Datum</th>
		<th>Soort contact</th>
		<th>Met</th>
		<th style="width: 500px;">Tekst</th>
		<th>Doorgeschoven</th>
	</tr>
<?php
	$recent = Moment::orderBy('datum', 'desc')->orderBy('updated_at', 'desc')->take(15)->get();
	foreach ($recent as $rec)
	{ 	setlocale(LC_ALL,'nl_NL.utf8');
		$contact = Contact::find($rec->contact_id);
		if (isset($contact))
		{
		?>
		<tr>
			<td class="nowrap"><a href="<?php echo e(url('contacten/' . $contact->id . '/edit#cm')); ?>"><?php echo e($contact->voornaam); ?> <?php echo e($contact->tussenvoegsel); ?> <?php echo e($contact->achternaam); ?></td>
			<td class="nowrap"><?php echo e($rec->datum); ?></td>
			<td><?php echo e(DB::table('la_contacttypes')->where('id', $rec->type)->value('type')); ?></td>
			<td><?php echo e(User::find($rec->gebruiker_id)->naam); ?></td>
			<td><div style="max-height: 85px; overflow: hidden;"><?php echo e($rec->tekst); ?></div></td>
			<td><?php if(isset($rec->doorschuif) && $rec->doorschuif > date('Y-m-d') && !$rec->klaar): ?>
						<?php if($rec->periode == 1): ?> <?php echo e(strftime('%e %B', strtotime($rec->doorschuif))); ?>

						<?php elseif($rec->periode == 2): ?> de week van <?php echo e(strftime('%e %B', strtotime($rec->doorschuif))); ?>

						<?php elseif($rec->periode == 3): ?> <?php echo e(strftime('%B', strtotime($rec->doorschuif))); ?>

						<?php endif; ?>
				<?php endif; ?></td>
		</tr>
	<?php }
	} ?>
</table>
<hr style="margin-top: 20px; border-bottom: 1px solid orange;"/>
<h2>Aankomende bijeenkomsten</h2>
<?php $bij1kn = Bijeenkomst::where('tijdstip', '>=', date('Y-m-d'))->orderBy('tijdstip')->get(); ?>
<table id="bijeenkomsten" width="75%">
<?php $__currentLoopData = $bij1kn; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bij1k): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<tr>
		<td><a href="<?php echo e(url('/evenementen/' . $bij1k->id . '/edit')); ?>"><?php echo e($bij1k->naam); ?></a></td>
		<td><?php echo e(strftime('%e %B %Y %H:%M',strtotime($bij1k->tijdstip))); ?>

				<?php if(isset($bij1k->eindtijd)): ?>
				-
					<?php if(substr($bij1k->tijdstip,0,10) == substr($bij1k->eindtijd,0,10)): ?>
						<?php echo e(strftime('%H:%M', strtotime($bij1k->eindtijd))); ?>

					<?php else: ?>
						<?php echo e(strftime('%e %B %Y %H:%M',strtotime($bij1k->eindtijd))); ?>

					<?php endif; ?>
				<?php endif; ?>     
		</td>
		<td>
			<?php
			$deelnemersPerType = DB::table('la_deelname')
				->join('la_deelnametypes', 'la_deelname.type_id', '=', 'la_deelnametypes.id')
				->select('type_id', 'type', DB::raw('count(contact_id) as aantal'))
				->where('bijeenkomst_id', '=', $bij1k->id)
				->groupBy('type_id')
				->groupBy('type')
				->get();
			$outputItems = array();
			foreach($deelnemersPerType as $dlnType) { $outputItems[] = $dlnType->type . ": " . $dlnType->aantal; }
			echo implode(", ", $outputItems);
			?>
		</td>
</td>
	</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>
<hr style="margin-top: 20px; border-bottom: 1px solid orange;"/>
<h2>Recent gewijzigde contactpersonen</h2>
<table id="recent">
	<tr><th>Naam</th><th>Laatst gewijzigd</th><th>door</th></tr>
<?php
	$recent = Contact::orderBy('updated_at', 'desc')->take(5)->get();
	foreach ($recent as $rec)
	{ ?>
		<tr>
			<td><a href="<?php echo e(url('contacten/' . $rec->id . '/edit')); ?>"><?php echo e($rec->voornaam); ?> <?php echo e($rec->tussenvoegsel); ?> <?php echo e($rec->achternaam); ?></td>
			<td class="nowrap"><?php echo e(substr($rec->updated_at,0,16)); ?></td>
			<td><?php echo e(User::find($rec->laatstgewijzigd)->naam); ?></td>
		</tr>
	<?php } ?>
</table>
<hr style="margin-top: 20px; border-bottom: 1px solid orange;"/>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
	<script src="<?php echo e(url('/js/app.js')); ?>"></script>
	<script src="<?php echo e(url('/js/jquery.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>