<?php /* /home/jos/git/aspidistra/lunamareadresboek/resources/views/organisaties/form.blade.php */ ?>
<?php $__env->startSection('title', 'Details organisatie'); ?>
<?php $__env->startSection('content'); ?>
<h3>Details <?php if(isset($organisatie)): ?> <?php echo e($organisatie->naam); ?> <?php else: ?> organisatie <?php endif; ?></h3>
<div id="app">
	<?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->yieldContent('content'); ?>
	<?php if($errors->any()): ?>
		<ul class="alert alert-danger">
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li ><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
	<?php endif; ?> 
</div>
<form action="<?php echo e($action); ?>" method="POST"  enctype="multipart/form-data">
<?php echo csrf_field(); ?>
<?php 
use App\Organisatie;
use App\Contact;
setlocale(LC_ALL, 'nl_NL.utf8');
	if (isset($organisatie->id) && $organisatie->id != 0)
	{ ?>
		<input type="hidden" name="_method" value="PUT">
<?php } else 
	{
		if (!isset($organisatie->id))
		{
			$organisatie = new Organisatie();
		}
	} ?>
<style>fieldset label {width: 115px;} </style>
<fieldset>
	<p>Velden met * zijn verplichte velden.</p>
	<p>
		<label for="naam">Naam*:</label> <input class="focushier" type='text' id='naam' name='naam' value='<?php echo e($organisatie->naam); ?>' required size="60"></input>
	</p>
	<p>
		<label for="plaats">Plaats:</label> <input type='text' id='plaats' name='plaats' value='<?php echo e($organisatie->plaats); ?>'></input>
	</p>
	<p>
		<label for="email">Email-adres:</label> <input type='email' id='email' name='email' value='<?php echo e($organisatie->email); ?>' size="50"></input>
	</p>
	<p>
		<label for="email">Telefoon:</label> <input type='text' id='telnr' name='telnr' value='<?php echo e($organisatie->telnr); ?>' size="50"></input>
	</p>
	<p>
		<label for="website">Website:</label> <input type='text' id='website' name='website' value='<?php echo e($organisatie->website); ?>' size="50"></input>
	</p>
	<p>
		<label for="opmerkingen">Opmerkingen:</label> <textarea rows="8" cols="80" id='opmerkingen' name='opmerkingen'><?php echo e($organisatie->opmerkingen); ?></textarea>
	</p>
	<button class="btn btn-primary waarsch">Niet gewijzigd</button>
	<?php if(isset($organisatie->id)): ?>
		<p>Bij <?php echo e($organisatie->naam); ?> kennen we deze contactpersonen:</p><p>
		<?php $orgconts = Contact::where('organisatie', $organisatie->naam)->orderBy('voornaam')->orderBy('achternaam')->get(); ?>
		<?php $__currentLoopData = $orgconts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orgcont): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<a href="<?php echo e(url('contacten/' . $orgcont->id . '/edit')); ?>"><strong><?php echo e($orgcont->helenaam()); ?></strong></a><br/>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</p>
	<?php endif; ?>
</fieldset>
</form>
<?php if(isset($organisatie->id)): ?>
<form id="verwijderen" method="POST" action = "<?php echo e(url('organisaties/' . $organisatie->id)); ?>">
	<?php echo csrf_field(); ?>
	<?php echo method_field('DELETE'); ?>
	<button class="onzichtbaar btn"
		teverwijderen="<?php echo e($organisatie->naam); ?>"
		>organisatie <?php echo e($organisatie->naam); ?> verwijderen</button>
</form>
<br/>
<?php endif; ?>
<p><a href="<?php echo e(url('organisaties')); ?>"><button class='btn btn-normaal'>&larr;Naar organisaties</button></a>
<a href="<?php echo e(url('organisaties/create')); ?>"><button class='btn btn-normaal'>Nieuwe organisatie</button></a></p>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
	<script src="<?php echo e(url('/js/app.js')); ?>"></script>
	<script src="<?php echo e(url('/js/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/ajaxfuncties.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>