<?php /* /home/jos/git/aspidistra/lunamareadresboek/resources/views/organisaties/index.blade.php */ ?>
<?php 
use App\Organisatie;
use App\Contact;
?>

<?php $__env->startSection('title', 'Organisaties'); ?>

<?php $__env->startSection('content'); ?>

<style>
	#organisaties {table-layout: fixed; width: 100% !important;}
	#organisaties td, #organisaties th {
		  width: auto !important;
		  white-space: normal;
		  text-overflow: ellipsis;
		  overflow: hidden;
	}

</style>
<h2>Organisaties</h2>
<div id="app">
	<?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->yieldContent('content'); ?>
</div>
<a href="<?php echo e(url('organisaties/create')); ?>"><button class='btn btn-normaal'>Nieuwe organisatie</button></a>
<table class="table table-striped table-bordered display compacts" style="width: auto;" id="organisaties">
    <thead>
        <tr>
			<td>Naam</td>
			<td>Plaats</td>
			<td>E-mail</td>
			<td>Telnr.</td>
			<td>Website</td>
			<td>Contact-<br/>personen</td>
			<td>Opmerkingen</td>
        </tr>
    </thead>
    <tbody>
	<?php 
		foreach ($organisaties as $organisatie) {
			$count = Contact::where('organisatie',$organisatie->naam)->count();
			?>
       <tr>
			<td style="font-weight: bold; white-space: nowrap;"><a href="<?php echo e(url('/organisaties/' . $organisatie->id . '/edit')); ?>"><?php echo e($organisatie->naam); ?></a></td>
			<td><?php echo e($organisatie->plaats); ?></td>
			<td><a href="mailto:<?php echo e($organisatie->email); ?>"><?php echo e($organisatie->email); ?></a></td>
			<td style="white-space: nowrap;"><?php echo e($organisatie->telnr); ?></td>
			<td><a href="http://<?php echo e($organisatie->website); ?>" target="_blank"><?php echo e($organisatie->website); ?></a></td></td>
			<td style="text-align: center;"><?php echo e($count); ?></td>
              <td><div style="max-height: 80px; overflow: hidden; text-overflow: ellipsis;"><?php echo e($organisatie->opmerkingen); ?></div></td>
       </tr>
	<?php } ?>
    </tbody>
</table>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
	<script src="<?php echo e(url('/js/app.js')); ?>"></script>
	<script src="<?php echo e(url('/js/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/jquery-ui.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/datatables.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/ajaxfuncties.js')); ?>"></script>
	<script>
$(function() {
	$('#organisaties').DataTable({'info': false, 'paging': false,language: {
        search: "Zoek in de lijst:",
    }, 'order': [[ 0, "asc" ]]
    });
 });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>