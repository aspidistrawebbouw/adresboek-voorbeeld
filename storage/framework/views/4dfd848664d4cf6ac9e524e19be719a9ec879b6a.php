<?php /* /home/jos/git/aspidistra/lunamareadresboek/resources/views/gebruikers/form.blade.php */ ?>
<?php $__env->startSection('title', 'Details gebruiker'); ?>
<?php $__env->startSection('content'); ?>
<h3>Details <?php if(isset($gebruiker)): ?> <?php echo e($gebruiker->naam); ?> <?php else: ?> gebruiker <?php endif; ?></h3>
<div id="app">
	<?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->yieldContent('content'); ?>
	<?php if($errors->any()): ?>
		<ul class="alert alert-danger">
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li ><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
	<?php endif; ?> 
</div>
<form action="<?php echo e($action); ?>" method="POST"  enctype="multipart/form-data">
<?php echo csrf_field(); ?>
<?php 
use App\Contact;
use App\Deelname;
use App\Moment;
use App\User;
setlocale(LC_ALL, 'nl_NL.utf8');
	if (isset($gebruiker->id) && $gebruiker->id != 0)
	{ ?>
		<input type="hidden" name="_method" value="PUT">
<?php } else 
	{
		if (!isset($gebruiker->id))
		{
			$gebruiker = new User();
		}
	} ?>
<style>fieldset label {width: 115px;} </style>
<fieldset>
	<?php if(isset($gebruiker->id)): ?>
		<div class="infoblokje">
			<p><strong>Info</strong></p>
			<p>Laatst ingelogd: <?php if(null == $gebruiker->last_login_at || $gebruiker->last_login_at == "0000-00-00 00:00:00"): ?> Nooit <?php else: ?> <?php echo e(strftime('%d %B %Y %H:%M',strtotime($gebruiker->last_login_at))); ?><?php endif; ?></p>
		</div>
	<?php endif; ?>
	<p>Velden met * zijn verplichte velden.</p>
	<p>
		<label for="naam">Naam*:</label> <input  class="focushier" type='text' id='naam' name='naam' value='<?php echo e($gebruiker->naam); ?>'></input>
	</p>
	<p>
		<label for="email">Email-adres*:</label> <input type='email' id='email' name='email' value='<?php echo e($gebruiker->email); ?>' size="50"></input>
	</p>
	<button class="btn btn-primary waarsch">Niet gewijzigd</button>
</fieldset>
</form>
<?php if(isset($gebruiker->id)): ?>
<form id="verwijderen" method="POST" action = "<?php echo e(url('gebruikers/' . $gebruiker->id)); ?>">
	<?php echo csrf_field(); ?>
	<?php echo method_field('DELETE'); ?>
	<button class="onzichtbaar btn"
		teverwijderen="<?php echo e($gebruiker->naam); ?>"
		>Gebruiker <?php echo e($gebruiker->voornaam); ?> verwijderen</button>
</form>
<br/>
<?php endif; ?>
<p><a href="<?php echo e(url('gebruikers')); ?>"><button class='btn btn-normaal'>&larr;Naar gebruikers</button></a>
<a href="<?php echo e(url('gebruikers/create')); ?>"><button class='btn btn-normaal'>Nieuwe gebruiker</button></a></p>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
	<script src="<?php echo e(url('/js/app.js')); ?>"></script>
	<script src="<?php echo e(url('/js/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/ajaxfuncties.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>