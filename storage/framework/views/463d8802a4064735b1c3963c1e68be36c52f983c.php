<?php /* /home/jos/git/aspidistra/lunamareadresboek/resources/views/evenementen/index.blade.php */ ?>
<?php 
use App\Bijeenkomst;
?>

<?php $__env->startSection('title', 'Bijeenkomsten'); ?>

<?php $__env->startSection('content'); ?>

<style>
	#bijeenkomsten {table-layout: fixed; width: 100% !important;}
	#bijeenkomsten td, #bijeenkomsten th {
		  width: auto !important;
		  white-space: normal;
		  text-overflow: ellipsis;
		  overflow: hidden;
	}

</style>
<h2>Bijeenkomsten</h2>
<div id="app">
	<?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->yieldContent('content'); ?>
</div>
<a href="<?php echo e(url('evenementen/create')); ?>"><button class='btn btn-normaal'>Nieuwe bijeenkomst</button></a>
<fieldset id="filters">
	<h4>Filters</h4>
	<table>
	<tr style="vertical-align: top;">
		<td style="padding-right: 20px;">Type bijeenkomst:</td>
		<td style="padding-right: 20px;"><button class="filter bronfilter allemaal" rubriek="lb">Allemaal</button>
		<button class="filter bronfilter geen" style="background-color: #d05050;" rubriek="lb">Geen</button></td>
		<td style="padding-right: 20px;">
		<?php 
		$i = 0;
		$typen = DB::table('la_bijeenkomsten')->select('type')->distinct()->get();
		foreach ($typen as $type)
		{ $i++; ?>
		<button class="filter bronfilter" rubriek="lb"><?php echo e($type->type); ?></button>
		<?php if($i % 5 == 0): ?> <br/> <?php endif; ?>
		<?php } ?>
		</td>
	</tr>
	</table>
</fieldset>
<hr>
<table class="table table-striped table-bordered display compacts" style="width: auto;" id="bijeenkomsten">
    <thead>
        <tr>
			<th>Naam</th>
			<th>Datum / tijd</th>
			<th>Type</th>
            <th>Opmerkingen</th>
            <th>Aantal deelnemers</th>
        </tr>
    </thead>
    <tbody>
	<?php 
		setlocale(LC_ALL,'nl_NL.utf8');
		$gepasseerd = 0;
		foreach ($bijeenkomsten as $bij1k) {
		?>
       <tr>
			<td style="white-space: nowrap; font-weight: bold;"><a href="<?php echo e(url('evenementen/' . $bij1k->id . '/edit')); ?>"><?php echo e($bij1k->naam); ?></a></td>
            <td data-sort="<?php echo e($bij1k->tijdstip); ?>"><?php echo e(strftime('%e %B %Y %H:%M',strtotime($bij1k->tijdstip))); ?>

				<?php if(isset($bij1k->eindtijd) & $bij1k->eindtijd != "0000-00-00 00:00:00"): ?>
				-
					<?php if(substr($bij1k->tijdstip,0,10) == substr($bij1k->eindtijd,0,10)): ?>
						<?php echo e(strftime('%H:%M', strtotime($bij1k->eindtijd))); ?>

					<?php else: ?>
						<?php echo e(strftime('%e %B %Y %H:%M',strtotime($bij1k->eindtijd))); ?>

					<?php endif; ?>
				<?php endif; ?>            
            </td>
			<td class="bron"><?php echo e($bij1k->type); ?></td>
            <td style="max-height: 40px;"><?php echo e(substr($bij1k->opmerkingen,0,100)); ?><?php if(strlen($bij1k->opmerkingen) > 99): ?>...<?php endif; ?></td>
            <td>
	<?php
			$deelnemersPerType = DB::table('la_deelname')
				->join('la_deelnametypes', 'la_deelname.type_id', '=', 'la_deelnametypes.id')
				->select('type_id', 'type', DB::raw('count(contact_id) as aantal'))
				->where('bijeenkomst_id', '=', $bij1k->id)
				->groupBy('type_id')
				->groupBy('type')
				->get();
			foreach($deelnemersPerType as $dlnType)
			{ ?>
				<?php echo e($dlnType->type); ?>: <?php echo e($dlnType->aantal); ?><br/>
			<?php } ?>
			</td>
        </tr>
	<?php } ?>
    </tbody>
</table>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
	<script src="<?php echo e(url('/js/app.js')); ?>"></script>
	<script src="<?php echo e(url('/js/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/jquery-ui.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/datatables.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/js.cookie.js')); ?>"></script>
	<script>
$(function() {
	$('#bijeenkomsten').DataTable({'info': false, 'paging': false,language: {
        search: "Zoek in de lijst:",
    }, 'order': [[ 1, "asc" ]]
    });
 });

</script>
	<script src="<?php echo e(url('/js/ajaxfuncties.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>