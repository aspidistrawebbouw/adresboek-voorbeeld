<?php /* /home/jos/git/aspidistra/lunamareadresboek/resources/views/gebruikers/index.blade.php */ ?>
<?php 
use App\User;
?>

<?php $__env->startSection('title', 'Gebruikers'); ?>

<?php $__env->startSection('content'); ?>

<style>
	#gebruikers {table-layout: fixed;}
	#gebruikers td, #gebruikers th {
		  white-space: normal;
		  text-overflow: ellipsis;
		  overflow: hidden;
	}

</style>
<h2>Gebruikers</h2>
<div id="app">
	<?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->yieldContent('content'); ?>
</div>
<a href="<?php echo e(url('gebruikers/create')); ?>"><button class='btn btn-normaal'>Nieuwe gebruiker</button></a>
<hr>
<table class="table table-striped table-bordered display compacts" style="width: 50%;" id="gebruikers">
    <thead>
        <tr>
			<th>Naam</th>
			<th>Email</th>
			<th>Laatste keer ingelogd</th>
        </tr>
    </thead>
    <tbody>
	<?php 
		setlocale(LC_ALL,'nl_NL.utf8');
		foreach ($gebruikers as $gebruiker) {
			?>
       <tr>
			<td style="white-space: nowrap; font-weight: bold;"><a href="<?php echo e(url('gebruikers/' . $gebruiker->id . '/edit')); ?>"><?php echo e($gebruiker->naam); ?></a></td>
			<td><?php echo e($gebruiker->email); ?></td>
            <td><?php if(null == $gebruiker->last_login_at || $gebruiker->last_login_at == "0000-00-00 00:00:00"): ?> Nooit <?php else: ?> <?php echo e(strftime('%d %B %Y %H:%M',strtotime($gebruiker->last_login_at))); ?><?php endif; ?></td>
        </tr>
	<?php } ?>
    </tbody>
</table>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
	<script src="<?php echo e(url('/js/app.js')); ?>"></script>
	<script src="<?php echo e(url('/js/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/jquery-ui.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/ajaxfuncties.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>