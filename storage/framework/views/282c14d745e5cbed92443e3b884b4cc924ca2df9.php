<?php /* /home/jos/git/aspidistra/lunamareadresboek/resources/views/evenementen/form.blade.php */ ?>
<?php $__env->startSection('title', 'Details Bijeenkomst'); ?>
<?php $__env->startSection('content'); ?>
<h3>Details <?php if(isset($bij1k)): ?> <?php echo e($bij1k->naam); ?><?php else: ?> bijeenkomst <?php endif; ?></h3>
<div id="app">
	<?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<?php echo $__env->yieldContent('content'); ?>
	<?php if($errors->any()): ?>
		<ul class="alert alert-danger">
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li ><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
	<?php endif; ?> 
</div>
<form action="<?php echo e($action); ?>" method="POST"  enctype="multipart/form-data">
<?php echo csrf_field(); ?>
<?php 
use Illuminate\Support\Facades\DB;
use App\Bijeenkomst;
use App\Contact;
use App\Deelname;
setlocale(LC_ALL, 'nl_NL.utf8');
	if (isset($bij1k->id) && $bij1k->id != 0)
	{ ?>
		<input type="hidden" name="_method" value="PUT">
<?php } else 
	{
		if (!isset($bij1k->id))
		{
			$bij1k = new Bijeenkomst();
		}
	} ?>
<style>fieldset label {width: 115px;} </style>
<fieldset>
	<p>Velden met * zijn verplichte velden.</p>
	<p>
		<label for="naam">Naam*:</label> <input  class="focushier" type='text' id='naam' name='naam' value='<?php echo e($bij1k->naam); ?>' size="50" required></input>
	</p>
	<p>
		<label for="datumtijd">Datum/tijd:</label> 
			<?php if (isset($bij1k->tijdstip) & $bij1k->tijdstip != "0000-00-00 00:00:00") 
			{
				$mydate = strtotime($bij1k->tijdstip);
				$datum = strftime("%Y-%m-%d",$mydate);
				$tijd = strftime("%H:%M", $mydate);
			} else 
			{
				$tijd = "10:00";
				$datum = date('Y-m-d');
			} ?>
			<input type="date" id="datumtijd" name="datum" value="<?php echo e($datum); ?>"></input>
			<input type="time" name="tijd" min="8:00" max="22:00" value="<?php echo e($tijd); ?>"></input>
		<label for="einddatumtijd">Eindtijd:</label> 
			<?php if (isset($bij1k->eindtijd) & $bij1k->eindtijd != "0000-00-00 00:00:00") 
			{
				$mydate = strtotime($bij1k->eindtijd);
				$datum = strftime("%Y-%m-%d",$mydate);
				$tijd = strftime("%H:%M", $mydate);
			} else if (isset($bij1k->tijdstip) & $bij1k->tijdstip != "0000-00-00 00:00:00")
			{
				$mydate = strtotime($bij1k->tijdstip);
				$datum = strftime("%Y-%m-%d",$mydate);
				$tijd = strftime("%H:%M", $mydate);
			} else 
			{
				$tijd = "12:00";
				$datum = date('Y-m-d');
			} ?>
			<input type="date" id="einddatumtijd" name="einddatum" value="<?php echo e($datum); ?>"></input>
			<input type="time" name="eindtijd" min="8:00" max="22:00" value="<?php echo e($tijd); ?>"></input>
	</p>
	<p>
		<label for="opmerkingen">Opmerkingen:</label> <textarea rows="8" cols="80" id='opmerkingen' name='opmerkingen'><?php echo e($bij1k->opmerkingen); ?></textarea>
	</p>
	<p>
		<label for="type">Type bijeenkomst:</label>
			<select id="type" name="type">
				<option value="" disabled <?php if(!isset($bij1k->type)): ?> selected <?php endif; ?>>Kies:</option>
			<?php $types = DB::table('la_bijeenkomsten')->select('type')->distinct()->get();
				foreach($types as $type)
				{ ?>
				<option value="<?php echo e($type->type); ?>" <?php if($bij1k->type == $type->type): ?> selected <?php endif; ?>><?php echo e($type->type); ?></option>
			<?php } ?>
				</select>
		<label for="typeanders">anders, nl:</label>
		<input id="typeanders" type="text" size="50" name="typeanders" value="<?php echo e($bij1k->typeanders); ?>"></input>
	</p>
<button class="btn btn-primary waarsch">Niet gewijzigd</button>

</fieldset>
</form>
<?php if(isset($bij1k->id)): ?>
<form id="verwijderen" method="POST" action = "<?php echo e(url('evenementen/' . $bij1k->id)); ?>">
	<?php echo csrf_field(); ?>
	<?php echo method_field('DELETE'); ?>
	<button class="onzichtbaar btn"
		teverwijderen="<?php echo e($bij1k->naam); ?>"
		>Bijeenkomst "<?php echo e($bij1k->naam); ?>" verwijderen</button>
</form>
<br/>
<h3 id="betrokken">Wie zijn/waren erbij betrokken?</h3>
<fieldset>
	<?php 
		$deelnames = Deelname::where('bijeenkomst_id', $bij1k->id)->orderBy('type_id')->get(); ?>
		<p>In totaal <?php echo e($deelnames->count()); ?> deelnemers.</p>

	<table style="width: 75%;">
		<thead>
			<th>Naam</th>
			<th>Datum</th>
			<th>Opmerkingen</th>
			<th>Wijzig</th>
			<th>Verwijder</th>
		</thead>
	<?php	
	$deelnames = Deelname::where('bijeenkomst_id', $bij1k->id)->orderBy('type_id')->get();
	$vorigeType = 0;
	foreach ($deelnames as $deelname)
	{   if ($deelname->type_id != $vorigeType) { 
			$vorigeType = $deelname->type_id;
			$type = DB::table('la_deelnametypes')->find($deelname->type_id); ?>
			<tr><td colspan="5"><strong><?php echo e($type->type); ?>:</strong></td></tr>
		<?php }
		$mycontact = Contact::find($deelname->contact_id);
		if (isset($mycontact)) { ?>
			<tr class="toonbijeenkomst" id="toon<?php echo e($deelname->id); ?>">
				<td style="padding-right: 20px; font-weight: bold;"><a href="<?php echo e(url('contacten/' . $mycontact->id . '/edit')); ?>"><?php echo e($mycontact->helenaam()); ?></a></td>
				<td style="padding-right: 20px;"><?php echo e($deelname->datum); ?></td>
				<td style="padding-right: 20px;"><?php echo e($deelname->opmerkingen); ?></td>
				<td style="text-align: center;"><input type="image" class="toonwijzig" item="<?php echo e($deelname->id); ?>" src="<?php echo e(asset('grafisch/wijzig.png')); ?>" width="15px" /></td>
				<td><a href="<?php echo e(url('deelname/verwijderen/' . $deelname->id)); ?>"><img src="<?php echo e(url('grafisch/delete.png')); ?>" width="15px"></a></td>
			</tr>
			<tr>
				<td colspan="5">
					<div id="wijzig<?php echo e($deelname->id); ?>" class="wijzigbijeenkomst">
						<form action="<?php echo e(url('/deelname/' . $deelname->id)); ?>" method="POST">
						<?php echo csrf_field(); ?>
						<input type="hidden" name="_method" value="PUT">
						<p><strong>Wijzig de deelname van <?php echo e($mycontact->helenaam()); ?> aan deze bijeenkomst:</strong></p>
						<p>Datum: <input type="date" name="datum" value="<?php echo e($deelname->datum); ?>"></input></p>
						<p>Opmerkingen: <input name="opmerkingen" size="50" value="<?php echo e($deelname->opmerkingen); ?>"></input>
						<p>Hoe: <select name="type_id" id="type_id">
							<?php 
								$types = DB::table('la_deelnametypes')->orderBy('id')->get();
								foreach ($types as $type)
								{ ?>
									<option value="<?php echo e($type->id); ?>" <?php if($deelname->type_id == $type->id): ?>) selected <?php endif; ?>><?php echo e($type->type); ?></option>
							<?php } ?>
								</select>
						</p>
						<button class="btn btn-primary" type="submit" style="float: right;" >Opslaan</button>
						</form>
						<button class="btn btn-normaal cancel" style="float: left;" item="<?php echo e($deelname->id); ?>">Annuleren</button>
					</div>
				</td>
				
			</tr>
	<?php	}
	} ?>
</table>
<hr/>
<p><strong>Deelnemer toevoegen:</strong></p>
	<form action="<?php echo e(url('/deelname/toevoegen')); ?>" method="POST">
	<?php echo csrf_field(); ?>
		<input type="hidden" name="bijeenkomst_id" value="<?php echo e($bij1k->id); ?>"></input>
		<label for="contact" style="width: auto; vertical-align: baseline;">Wie:</label>
		<select name="contact_id" id="contact">
			<option disabled selected>Kies:</option>
		<?php
			$contacten = Contact::orderBy('voornaam')->orderBy('tussenvoegsel')->orderBy('achternaam')->get();
			foreach ($contacten as $contact)
			{ ?>
				<option value="<?php echo e($contact->id); ?>"><?php echo e($contact->helenaam()); ?></option>
			<?php } ?>
		</select>			
		<label for="type_id" style="width: auto; vertical-align: baseline;">Wat:</label>
		<select name="type_id" id="type_id">
		<?php
			$types = DB::table('la_deelnametypes')->orderBy('id')->get();
			foreach ($types as $type)
			{ ?>
				<option value="<?php echo e($type->id); ?>" <?php if(isset($vorigeTypeId) && $vorigeTypeId == $type->id): ?>) selected <?php endif; ?>><?php echo e($type->type); ?></option>
			<?php } ?>
		</select>
		<label for="datum" style="width: auto; vertical-align: baseline;">Datum:</label>
		<input name="datum" type="date" id="datum" value="<?php echo e(date('Y-m-d')); ?>"></input>
		<label for="opmerkingen" style="width: auto; vertical-align: baseline;">Opmerking:</label>
		<input name="opmerkingen" type="text" id="opmerkingen">
		<button class="btn btn-primary" type="submit">Toevoegen</button>
	</form>
</fieldset>
<?php
		$voorkomende_types = Deelname::where('bijeenkomst_id', $bij1k->id)->pluck('type_id');
		$types = DB::table('la_deelnametypes')->whereIn('id', $voorkomende_types)->distinct()->get();
		?>
<?php if($types->count() > 0): ?>
<h3 id="export">Exporteren email-adressen</h3>
<p>Vink aan van welke categorieën deelnemers je de emailadressen wilt exporteren.</p>
<fieldset>
		<form action="<?php echo e(url('/evenement/exporteerEmail/' . $bij1k->id)); ?>" method="POST">
	<?php echo csrf_field(); ?>
	<?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<input type="checkbox" name="type[]" value="<?php echo e($type->id); ?>" 
			<?php if (!isset($vorigeCheckboxes) || in_array($type->id, $vorigeCheckboxes)) { ?> checked <?php } ?>	><?php echo e($type->type); ?></input><br/>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<p>&nbsp;</p>
		<p>Emailadressen scheiden met:
			<input type="radio" name="scheiden[]" value="1" <?php if(!isset($scheiden) || $scheiden == 1): ?> checked <?php endif; ?>>Komma</input>
			<input type="radio" name="scheiden[]" value="2" <?php if(isset($scheiden) && $scheiden == 2): ?> checked <?php endif; ?>>Puntkomma</input>
			<input type="radio" name="scheiden[]" value="3" <?php if(isset($scheiden) && $scheiden == 3): ?> checked <?php endif; ?>>Geen</input> </p>
		<button class="btn btn-primary" type="submit">Exporteer</button></a>
	</form>
	<?php if(isset($exportoutput)): ?>
		<hr style="clear: both;"/>
		<pre id="exportlijst"><?php echo e($exportoutput); ?> </pre>
	<?php endif; ?>
</fieldset>
<?php endif; ?>

<?php endif; ?>

<p><a href="<?php echo e(url('evenementen')); ?>"><button class='btn btn-normaal'>&larr;Naar bijeenkomsten</button></a>
<a href="<?php echo e(url('evenementen/create')); ?>"><button class='btn btn-normaal'>Nieuwe bijeenkomst</button></a></p>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
	<script src="<?php echo e(url('/js/app.js')); ?>"></script>
	<script src="<?php echo e(url('/js/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(url('/js/ajaxfuncties.js')); ?>"></script>
	<?php if(isset($scroll)): ?>
	<script>
		document.getElementById('<?php echo e($scroll); ?>').scrollIntoView();
	</script>
	<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>