$(document).ready(function(){

	$('.klaarmelding').click(function(e){
	   e.preventDefault();
	   $.ajaxSetup({
		  headers: {
			  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		  }
	  });
	  var thisbutton = $(this);
	  var url = $(this).attr('url');
	  console.log("klaarmelden van " + $( this ).attr('moment_id'));
	   $.ajax({
		  url: url,
		  method: 'get',
		  success: function(result){
			 console.log(result);
			thisbutton.parent().html('<span class="okeevinkje">&#x2714;</span>');
		  }});
	   });

// Filterknoppen instellen op basis van cookies (indien aanwezig)

	$( '.bronfilter' ).click(function(e) {
		var rubriek = $( this ).attr('rubriek');
		$( '.dataTable tbody tr td.bron:contains(' +  $( this ).text() + ')' ).parent().toggle();
		$( this ).toggleClass('onzichtbaar');
		var cookieStatus = Cookies.get(rubriek + '_' + $( this ).text());
		cookieStatus = 1 - cookieStatus;
		Cookies.set(rubriek + '_' + $( this ).text(), cookieStatus);
		$('#counter').text($( '.dataTable tbody tr:visible').length);
	});
	
	$( '.bronfilter.allemaal' ).click(function(e) {
		var rubriek = $( this ).attr('rubriek');
		$ (' .dataTable tbody tr').show();
		$('.bronfilter').removeClass('onzichtbaar');
		$('.bronfilter').each(function () {
			Cookies.set(rubriek + '_' + $( this ).text(), 1);
		});
		$('#counter').text($( '.dataTable tbody tr:visible').length);
	});
	$( '.bronfilter.geen' ).click(function(e) {
		var rubriek = $( this ).attr('rubriek');
		$ (' .dataTable tbody tr').hide();
		$('.bronfilter').addClass('onzichtbaar');
		$('.bronfilter.geen').removeClass('onzichtbaar');
		$('.bronfilter.allemaal').removeClass('onzichtbaar');
		$('.bronfilter').each(function () {
			Cookies.set(rubriek + '_' + $( this ).text(), 0);
		});
		$('#counter').text($( '.dataTable tbody tr:visible').length);
	});
		 
	// Initiële situatie:
		$(' .bronfilter' ).each(function () {
			var rubriek = $( this ).attr('rubriek');
			setKnopVolgensCookie( $( this ).text() , rubriek);
		});
		
function setKnopVolgensCookie (knopstring, rubriek) {
	if (knopstring == 'Geen') {
	} else if (knopstring == 'Allemaal') {
	} else {
		var cookieStatus = Cookies.get(rubriek + '_' + knopstring);
		if (typeof cookieStatus == 'undefined') {
	// alles tonen 
			$( '.dataTable tbody tr td.bron:contains(' + knopstring + ')').parent().show();
			$( '.bronfilter:contains(' + knopstring + ')').removeClass('onzichtbaar');
			Cookies.set(rubriek + '_' + knopstring, 1);
		} else {
			if (cookieStatus == 1) {
				$( '.dataTable tbody tr td.bron:contains(' + knopstring + ')').parent().show();
				$( '.bronfilter:contains(' + knopstring + ')').removeClass('onzichtbaar');
			} else if (cookieStatus == 0) {
				$( '.dataTable tbody tr td.bron:contains(' + knopstring + ')').parent().hide();
				$( '.bronfilter:contains(' + knopstring + ')').addClass('onzichtbaar');
			}
		}
	}			
}

// Resulterend aantal ongefilterde rijen tonen

$('#counter').text($( '.dataTable tbody tr:visible').length);

$('#verwijderen').submit(function() {
	var teverwijderen = $(this).find('button').attr('teverwijderen');
    var c = confirm('Weet je zeker dat je "' + teverwijderen + '" wilt verwijderen? Dit kan niet ongedaan worden gemaakt.');
    return c;
});

// Een tag verwijderen

$('.tagbutton').click(function(e) {
	var url = $( this ).attr('url');
	e.preventDefault();
	  $.ajaxSetup({
		  headers: {
			  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		  }
	  });
	  var thisbutton = $(this);
	  console.log("verwijderen van tag " + url);
	   $.ajax({
		  url: url,
		  method: 'get',
		  success: function(result){
			 console.log(result);
			thisbutton.parent().remove();
		  }});
	   });


if (document.location.hash != '#cm') {
	$('.focushier').attr('autofocus', 'autofocus');
}

$( '.toonwijzig').click(function(e) {
	e.preventDefault();
	var itemid = $(this).attr('item');
	$('#wijzig' + itemid).slideDown(800);
	$('#toon' + itemid).hide();
	});

$( 'button.cancel').click(function(e) {
	e.preventDefault();
	var itemid = $(this).attr('item');
	$('#wijzig' + itemid).slideUp(800);
	$('#toon' + itemid).show();
	});

var zoekstring = Cookies.get('zoekstring');
$( '#contacten_filter input').val(zoekstring);
var mytable = $('#contacten').DataTable();
mytable.search(zoekstring).draw();

$( '#contacten_filter input').change(function() {
	Cookies.set('zoekstring', $(this).val() );
	$('#counter').text($( '.dataTable tbody tr:visible').length);
	});


});

$(':input').on("input", function () {$(this).closest('form').find('button.waarsch').css('background-color', '#e3342f').text("Opslaan")});
