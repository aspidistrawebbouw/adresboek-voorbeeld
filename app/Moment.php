<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moment extends Model
{
    protected $fillable = ['contact_id', 'gebruiker_id', 'type', 'tekst', 'datum', 
		'doorschuif', 'periode', 'klaar'];
    protected $table = 'la_momenten';

}

/*  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `gebruiker_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `tekst` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `datum` date DEFAULT NULL,
*/
