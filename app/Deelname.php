<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Deelname extends Model
{
    protected $fillable = ['bijeenkomst_id' , 'contact_id' , 'type_id', 'opmerkingen'];
    protected $table = 'la_deelname';

	public function deelnemers() 
	{
		return $this->hasMany('App\Contact');
	}
	
	public function bijeenkomsten() 
	{
		return $this->hasMany('App\Bijeenkomst');
	}
	
	public function type()
	{
		$deelnametype = DB::table('la_deelnametypes')->where('id', $this->type_id)->first();
		return $deelnametype->type;
	}
	
	public function bijeenkomst()
	{   $bijeenkomst = Bijeenkomst::find($this->bijeenkomst_id);
		return $bijeenkomst->naam;
	}
	
	public function deelnemer()
	{
		return Contact::find($this->contact_id)->helenaam();
	}
}

/* 
 *CREATE TABLE `la_deelname` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  bijeenkomst_id integer,
  contact_id integer,
  type_id integer,
  opmerkingen varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

*/
