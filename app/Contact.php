<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tag;

class Contact extends Model
{
    protected $fillable = ['voornaam' , 'tussenvoegsel' , 'achternaam',
			'email' , 'locatie' , 'organisatie', 'functie', 'opmerkingen', 'herkomst',
			'created_at' , 'updated_at' , 'telnr' , 'geboortedatum', 'datum_bestelling'];
    protected $table = 'la_contacten';

	public function momenten() 
	{
		return $this->hasMany('App\Moment');
	}
	
	public function laatsteContactMoment()
	{
		$maxmom = $this->momenten()->max('datum');
		return $maxmom;
	}
	
	public function bijeenkomsten()
	{
		return $this->belongsToMany('App\Bijeenkomst', 'la_deelname');
	}
	
	public function helenaam() 
	{ 
		return implode(" ", array($this-> voornaam,$this->tussenvoegsel,$this->achternaam));
	}
	
	public function tags()
	{
		return Tag::where('contact_id',$this->id)->get();
	}
		
}

/* 
 *`voornaam` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tussenvoegsel` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `achternaam` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locatie` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organisatie` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opmerkingen` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `herkomst` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `telnr` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datum_bestelling` date DEFAULT NULL,

*/
