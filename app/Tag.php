<?php

namespace App;
use App\Contact;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{
    protected $fillable = ['tag_id' , 'contact_id'];
    protected $table = 'la_contacttags';

	public function tagtekst()
	{
		return TagTekst::find($this->tag_id)->tekst;
	}
	
	public function heleNaam()
	{
		$contact = Contact::find($this->contact_id);
		if (isset($contact->id)) return Contact::find($this->contact_id)->helenaam();
		else return "Onbekend (debug)";
	}

	public function kleur()
	{
		$tag = TagTekst::find($this->tag_id);
		return $tag->kleur();
	}
	
	public $timestamps = false;
}
