<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bijeenkomst extends Model
{
    protected $fillable = ['naam', 'type', 'tijdstip', 'eindtijd', 'opmerkingen'];
    protected $table = 'la_bijeenkomsten';
    
	public function deelnemers()
	{
		return $this->belongsToMany('App\Contact','la_deelname');
	}
	
	public function deelnames() {
		return $this->hasMany('App\Deelname','bijeenkomst_id');
	}
}
