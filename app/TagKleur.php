<?php

namespace App;
use App\Contact;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TagKleur extends Model
{
    protected $fillable = ['kleur'];
    protected $table = 'la_tagkleuren';
	public $timestamps = false;
}
