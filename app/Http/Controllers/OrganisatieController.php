<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Organisatie;
use DateTime;

class OrganisatieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organisaties = Organisatie::orderBy('naam')->get();
       	return View::make('organisaties.index')
            ->with('organisaties', $organisaties);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return View::make('organisaties.form')
			->with('action', url('organisaties'));
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $organisatie = new Organisatie();
		$organisatie->naam = substr($request->naam,0,69);
		$organisatie->email = substr($request->email,0,69);
		$organisatie->plaats = substr($request->plaats,0,69);
		$organisatie->telnr =  substr($request->telnr,0,29);
		$organisatie->opmerkingen = substr($request->opmerkingen, 0,499);
		$organisatie->website = substr(str_replace("http://","",str_replace("https://","",$request->website)),0,59);
		$organisatie->save();
		return redirect()->action('OrganisatieController@index')->with('success' , 'Organisatie ' . $organisatie->naam . ' opgeslagen');   

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       		Log::info("O12 Edit-scherm van organisatie " . $id);
		return View::make('organisaties.form')
			->with('organisatie', Organisatie::find($id))
			->with('action', url('organisaties/' . $id));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $organisatie = Organisatie::find($id);
		$organisatie->naam = substr($request->naam,0,69);
		$organisatie->email = substr($request->email,0,69);
		$organisatie->plaats = substr($request->plaats,0,69);
		$organisatie->telnr =  substr($request->telnr,0,29);
		$organisatie->website = substr(str_replace("http://","",str_replace("https://","",$request->website)),0,59);
		$organisatie->opmerkingen = substr($request->opmerkingen, 0,499);
		$organisatie->save();
		$organisaties = Organisatie::orderBy('naam')->get();
             return View::make('organisaties.index')
				->with('organisaties', $organisaties)
				->with('success', 'Organisatie ' . $organisatie->naam . ' opgeslagen');

    }

    public function destroy($id)
    {
        $organisatie = User::find($id);
        $organisatie->delete();
        return redirect()->action('OrganisatieController@index')->with('success', 'Organisatie ' . $organisatie->naam . ' verwijderd');       
     }
}
