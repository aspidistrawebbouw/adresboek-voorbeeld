<?php
namespace App\Http\Controllers;
use App\Deelname;
use App\Bijeenkomst;
use App\Contact;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class DeelnameController extends Controller
{
	public function index()
	{     
		Log::info(Auth::user()->naam . ' D01 DeelnameController index');
	}
	
	public function create()
	{
		Log::info(Auth::user()->naam . ' D06 DeelnameController create');
	}
	
	public function store()
	{
		Log::info(Auth::user()->naam . ' D11 DeelnameController store');
	}
		
   public function show($id)
    {
 		Log::info(Auth::user()->naam . ' D16 DeelnameController show ' . $id);
       // nooit gebruikt
    }
		
	public function edit($id)
    {
		Log::info(Auth::user()->naam . ' D21 DeelnameController edit ' . $id);
    } 

    public function update(Request $request, $id)
    {
		Log::info(Auth::user()->naam . ' D26 DeelnameController update ' . $id);
        $deelname = Deelname::find($id);
        $this->vul($request, $deelname);
        $deelname->save();
        $bij1k = Bijeenkomst::find($deelname->bijeenkomst_id);

		return redirect()->action('BijeenkomstController@edit', ['id' => $bij1k->id ] )->with('success' , 'Deelname van ' . Contact::find($deelname->contact_id)->helenaam() . ' aan deze bijeenkomst opgeslagen');
    }

   public function destroy($id)
    {
		Log::info(Auth::user()->naam . ' D51 Item ' . $id . ' verwijderen');
    }

	private function vul($request, &$mydeelname)
	{
		$mydeelname->opmerkingen = substr($request->opmerkingen, 0, 499);
		$mydeelname->type_id = $request->type_id;
		$mydeelname->datum = $request->datum;
	}
	
}
