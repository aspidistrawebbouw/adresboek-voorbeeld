<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\User;
use DateTime;

class GebruikerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
   		Log::info(Auth::user()->naam . ' G01 GebruikerController index');
       $gebruikers = User::orderBy('naam')->get();
       	return View::make('gebruikers.index')
            ->with('gebruikers', $gebruikers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		Log::info(Auth::user()->naam . ' G06 GebruikerController create');
		return View::make('gebruikers.form')
			->with('action', url('gebruikers'));


     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		Log::info(Auth::user()->naam . ' G11 GebruikerController store');
        $gebruiker = new User;
		$gebruiker->naam = $request->naam;
		$gebruiker->email = $request->email;
		$gebruiker->password = "initieel";
		$gebruiker->save();
		return redirect()->action('GebruikerController@index')->with('success' , 'Gebruiker ' . $gebruiker->naam . ' opgeslagen');   

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
  		Log::info(Auth::user()->naam . ' G16 GebruikerController show ' . $id);
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 		Log::info(Auth::user()->naam . ' G21 GebruikerController edit ' . $id);
		return View::make('gebruikers.form')
			->with('gebruiker', User::find($id))
			->with('action', url('gebruikers/' . $id));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
 		Log::info(Auth::user()->naam . ' G26 GebruikerController update ' . $id);
       $gebruiker = User::find($id);
		$gebruiker->naam = $request->naam;
		$gebruiker->email = $request->email;
		$gebruiker->save();
        $gebruikers = User::all();
             return View::make('gebruikers.index')
				->with('gebruikers', $gebruikers)
				->with('success', 'Gebruiker ' . $gebruiker->naam . ' opgeslagen');

    }

    public function destroy($id)
    {
		Log::info(Auth::user()->naam . ' G51 Gebruiker ' . $id . ' verwijderen');
        $gebruiker = User::find($id);
        $gebruiker->delete();
        return redirect()->action('GebruikerController@index')->with('success', 'Gebruiker ' . $gebruiker->naam . ' verwijderd');       
     }
     
     public function apiIndex() {
		 $gebruikers= User::all();
		 $output = 'adresboek ';
		 foreach ($gebruikers as $gebr)
		 {
			 $output .= $gebr->naam . ' - ' . $gebr->last_login_at . ' ' . $gebr->last_login_ip . '<br/>';
		 }
		 return $output;
	 }
}
