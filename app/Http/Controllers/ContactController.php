<?php

namespace App\Http\Controllers;
use App\Contact;
use App\Moment;
use App\Deelname;
use App\User;
use App\Tag;
use App\TagTekst;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info(Auth::user()->naam . ' C01 ContactController index');
        $contacten = Contact::orderBy('achternaam')->get();
        return View::make('contacten.index')
            ->with('contacten', $contacten);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		Log::info(Auth::user()->naam . ' C06 ContactController create');
		return View::make('contacten.form')
			->with('action', url('contacten'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		Log::info(Auth::user()->naam . ' C11 ContactController store');
		$foutmeldingen = [
			'required' => 'Dit veld moet ingevuld zijn.',
			'regex' => 'Telnr. mag alleen cijfers, "+()-" en spaties bevatten.',
			'email' => 'Dit is geen geldig e-mailadres'
		];
		$regels = [
			'achternaam' => 'required',
			'email' => 'nullable|email',
			'telnr' => 'nullable|regex:/^[0-9 \-+\(\)]{10,16}$/',
		];
		$validator = Validator::make($request->all(), $regels, $foutmeldingen);
		if ($validator->fails()) {
			Log::info(Auth::user()->naam . ' C13 ContactController store validatiefout');
			$errors = $validator->errors();
			$request->id = 0; // fake
			return View::make('contacten.form')
				->withErrors($validator)
				->with('contact', $request)
				->with('action', url('contacten'));
       }
       $contact = new Contact();
       $this->vul($request, $contact);
       $contact->bron = 'App';
       $contact->save();
              
       if (isset($request->nieuweBestaandeTag))
       {
		   $nbt = new Tag();
		   $nbt->tag_id = $request->nieuweBestaandeTag;
		   $nbt->contact_id = $contact->id;
		   $nbt->save();
	   }
       if (isset($request->nieuweTag) && $request->nieuweTag != '')
       {   $nt = new TagTekst();
		   $nt->tekst = $request->nieuweTag;
		   $nt->save();
		   
		   $nbt = new Tag();
		   $nbt->tag_id = $nt->id;
		   $nbt->contact_id = $contact->id;;
		   $nbt->save();
	   }


       return View::make('contacten.form')
			->with('contact', $contact)
			->with('action', url('contacten/' . $contact->id))
			->with('success', 'Gegevens ' . $contact->voornaam . ' ' . $contact->tussenvoegsel . ' ' . $contact->achternaam . ' opgeslagen');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
  		Log::info(Auth::user()->naam . ' C16 ContactController show ' . $id);
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		Log::info(Auth::user()->naam . ' C21 ContactController edit ' . $id);
		return View::make('contacten.form')
			->with('contact', Contact::find($id))
			->with('action', url('contacten/' . $id));
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		Log::info(Auth::user()->naam . ' C26 ContactController update ' . $id);
		$foutmeldingen = [
			'required' => 'Dit veld moet ingevuld zijn.',
			'regex' => 'Telnr. mag alleen cijfers, "-" en spaties bevatten.',
			'email' => 'Dit is geen geldig e-mailadres'
		];
		$regels = [
			'achternaam' => 'required',
			'email' => 'nullable|email',
			'telnr' => 'nullable|regex:/^[0-9 \-+\(\)]{10,16}$/',
		];
		$validator = Validator::make($request->all(), $regels, $foutmeldingen);
        $contact = Contact::find($id);
		if ($validator->fails()) {
			Log::info(Auth::user()->naam . ' C28 ContactController edit validatiefout ' . $id);
//			$errors = $validator;
            return View::make('contacten.form')
					->withErrors($validator)
                    ->with('contact', $contact)
					->with('action', url('contacten/' . $id));
        }
       $this->vul($request, $contact);
       $contact->save();
       
       if (isset($request->nieuweBestaandeTag))
       {
			Log::info(Auth::user()->naam . ' C31 ContactController edit tag ' . $request->nieuweBestaandeTag . ' toevoegen aan ' . $id);
		   // Is de tag al toegevoegd aan deze gebruiker?
		   $nbt = Tag::where('tag_id',$request->nieuweBestaandeTag)->where('contact_id',$id)->get();
		   if (null == $nbt || $nbt->count() == 0)
		   {
			    $nbt = new Tag();
			    $nbt->tag_id = $request->nieuweBestaandeTag;
			    $nbt->contact_id = $id;
			    $nbt->save();
				Log::info(Auth::user()->naam . ' C32 ContactController edit tag ' . $request->nieuweBestaandeTag . ' toegevoegd aan ' . $id);
		   } 
		   else Log::info(Auth::user()->naam . ' C33 ContactController edit contact ' . $id . ' heeft al tag ' . $request->nieuweBestaandeTag . ', doe niets');
	   }
	   
	   
       if (isset($request->nieuweTag) && $request->nieuweTag != '')
       {   
   			Log::info(Auth::user()->naam . ' C35 ContactController edit nieuwe tagtekst ' . $request->nieuweTag . ' toevoegen aan ' . $id);
		   $nt = TagTekst::where('tekst',$request->nieuweTag)->first();
		   if (null == $nt || $nt->count() == 0)
		   {
				$nt = new TagTekst();
				$nt->tekst = $request->nieuweTag;
				$nt->save();
				Log::info(Auth::user()->naam . ' C36 ContactController edit nieuwe tagtekst ' . $request->nieuweTag . ' opgeslagen');
			} else 
			{
				Log::info(Auth::user()->naam . ' C34 ContactController edit tagtekst ' . $request->nieuweTag . ' bestaat al, doe niets');
			}
		   
		   $nbt = Tag::where('tag_id',$nt->id)->where('contact_id',$id)->get();
		   if (null == $nbt || $nbt->count() == 0)
		   {
			   $nbt = new Tag();
			   $nbt->tag_id = $nt->id;
			   $nbt->contact_id = $id;
			   $nbt->save();
				Log::info(Auth::user()->naam . ' C38 ContactController edit tag ' . $request->nieuweTag . ' toegevoegd aan ' . $id);
 		   } 
		   else Log::info(Auth::user()->naam . ' C39 ContactController edit contact ' . $id . ' heeft al tag ' . $request->nieuweTag . ', doe niets');
   	   }

       return View::make('contacten.form')
			->with('contact', $contact)
			->with('action', url('contacten/' . $id))
			->with('success', 'Gegevens ' . $contact->voornaam . ' ' . $contact->tussenvoegsel . ' ' . $contact->achternaam . ' opgeslagen');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		Log::info(Auth::user()->naam . ' C51 ContactController destroy ' . $id);
       // delete
        $contact = Contact::find($id);
        $contact->delete();
		
		$momenten = Moment::where('contact_id',$id)->get();
		foreach ($momenten as $moment)
		{
			$moment->delete();
		}
        $deelnames = Deelname::where('contact_id', $id)->get();
        foreach ($deelnames as $deelname)
		{
			$deelname->delete();
		}
        $tags = Tag::where('contact_id', $id)->get();
        foreach ($tags as $tag)
		{
			$tag->delete();
		}

        // redirect
        return redirect()->action('ContactController@index')->with('success', 'Contactpersoon ' . $contact->voornaam . " " . $contact->tussenvoegsel . ' ' . $contact->achternaam . ' verwijderd');   

    }

   public function moment_verwijderen($id)
    {
		$moment = Moment::find($id);
		$contact = $moment->contact_id;
		$moment->delete();
        return redirect()->action('ContactController@edit',[ 'contact' => $contact ])->with('success', 'Contactmoment verwijderd');   
	}

	public function meldklaar($id)
	{
		$moment = Moment::find($id);
		$moment->klaar = 1;
		$moment->save();
		return "Agendapunt " . $id . " klaargemeld";
	}
	
	public function verwijdertag($id)
	{
		$tag = Tag::find($id);
		if ($tag) $tag->delete();
		return "OK";
	}
	
	private function vul($request, &$mycontact)
	{
	   $mycontact->voornaam = substr($request->voornaam, 0, 29);
	   $mycontact->tussenvoegsel = substr($request->tussenvoegsel, 0, 14);
	   $mycontact->achternaam = substr($request->achternaam, 0, 49);
	   $mycontact->email = substr(strtolower($request->email), 0, 69);
	   $mycontact->locatie = substr($request->locatie, 0, 49);
	   $mycontact->organisatie = substr($request->organisatie, 0, 49);
	   $mycontact->functie = substr($request->functie, 0,49);
	   $mycontact->opmerkingen = substr($request->opmerkingen, 0, 499);
// Herkomst
	   Log::info(Auth::user()->naam . ' C91 Herkomst: '. $request->herkomst);
	   Log::info(Auth::user()->naam . ' C92 Herkomst anders: '. $request->herkomstanders);
	    if (isset($request->herkomstanders) && null != $request->herkomstanders)
			$mycontact->herkomst = substr($request->herkomstanders, 0, 99);
		else if (isset($request->herkomst) && null != $request->herkomst && $request->herkomst != '(geen)')
				$mycontact->herkomst = $request->herkomst;
			else $mycontact->herkomst = null;
			
	   $mycontact->telnr = substr($request->telnr, 0, 49);
	   $mycontact->laatstgewijzigd = Auth::user()->id;
	   if (isset($request->geboortedatum))
			$mycontact->geboortedatum = $request->geboortedatum;
	   if (isset($request->datum_bestelling))
			$mycontact->datum_bestelling = substr($request->datum_bestelling, 0, 49);
	}
	
	public function bijeenkomst_toevoegen(Request $request) {
		$contact = Contact::find($request->contact_id);
		if ($request->bijeenkomst_id)
		{
			Log::info(Auth::user()->naam . ' C61 Deelname  ' . $request->contact_id . ' toevoegen aan bijeenkomst ' . $request->bijeenkomst_id);
			$deelname = new Deelname();
			$deelname->contact_id = $request->contact_id;
			$deelname->bijeenkomst_id = $request->bijeenkomst_id;
			$deelname->type_id = $request->type_id;
			$deelname->datum = $request->datum;
			$deelname->opmerkingen = substr($request->opmerkingen,0,499);
			$deelname->save();
			Log::info("C62 Deelname opgeslagen met type " . $request->type_id);
		   return View::make('contacten.form')
				->with('contact', $contact)
				->with('action', url('contacten/' . $contact->id))
				->with('success', 'Contact ' . $contact->helenaam() . ' toegevoegd aan bijeenkomst');
		}
		else return View::make('contacten.form')
				->with('contact', $contact)
				->with('action', url('contacten/' . $contact->id));
	}

    public function bijeenkomst_verwijderen($id)
    {
		$deelname = Deelname::find($id);
		Log::info(Auth::user()->naam . ' C63 Deelname  ' . $deelname->deelnemer() . ' verwijderen van bijeenkomst ' . $deelname->bijeenkomst());
		$bijeenkomst_id = $deelname->bijeenkomst_id;
		$contact = Contact::find($deelname->contact_id);
		$deelname->delete();
		Log::info("B57 Deelname $id verwijderd");
       return View::make('contacten.form')
			->with('contact', $contact)
			->with('action', url('contacten/' . $contact->id))
			->with('success', 'Contact ' . $contact->helenaam() . ' verwijderd van bijeenkomst');
	}

	public function apiStore(Request $request) {
		Log::info(Auth::guard('api')->user()->naam . ' CA01 Contact toevoegen');
	//	Log::info('API' . ' CA01 Contact toevoegen');
		if (Contact::where('email',$request->email)->count() == 0)
	//  Nog niemand met dit emailadres?
		{
			$apicontact = new Contact();
			$apicontact->voornaam = $request->voornaam;
			$apicontact->achternaam = $request->achternaam;
			$apicontact->email = $request->email;
			$apicontact->laatstgewijzigd = Auth::guard('api')->user()->id;
			$apicontact->bron = 'Formulier';
			$apicontact->save();
			return "OK - contact opgeslagen";
		} else return "Contact bestaat al in het adresboek";
	}

}
