<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use Validator;
use App\Bijeenkomst;
use App\Contact;
use App\Deelname;
use App\Moment;
use DateTime;

class BijeenkomstController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$bijeenkomsten = Bijeenkomst::all();
		Log::info(Auth::user()->naam . ' B01 BijeenkomstController index');

       	return View::make('evenementen.index')
            ->with('bijeenkomsten', $bijeenkomsten);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		Log::info(Auth::user()->naam . ' B06 BijeenkomstController create');
       		return View::make('evenementen.form')
			->with('action', url('evenementen'));

     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		Log::info(Auth::user()->naam . ' B11 BijeenkomstController store');
		$bij1k = new Bijeenkomst();
		$foutmeldingen = [
			'required' => 'Dit veld moet ingevuld zijn.',
			'max' => 'Graag een langere naam invullen'];
		$regels = [
			'naam' => 'required|min:1'
		];
		$validator = Validator::make($request->all(), $regels, $foutmeldingen);
		if ($validator->fails()) {
			$errors = $validator->errors();
			Log::info(Auth::user()->naam . ' B13 BijeenkomstController store validatiefout');
            return back()->withErrors($validator)
                        ->with('bij1k', $request)
						->with('action', url('evenementen'));
        }

       $bij1k->naam = substr($request->naam,0,49);
       $bij1k->tijdstip = $request->datum . " " . $request->tijd;
       $bij1k->eindtijd = $request->einddatum . " " . $request->eindtijd;
       $bij1k->opmerkingen = substr($request->opmerkingen,0,499);
       if (isset($request->typeanders)) $bij1k->type = substr($request->typeanders,0,39);
		else if (isset($request->type)) $bij1k->type = $request->type;
					 else $bij1k->type = 'geen';
		Log::info("B07 Bijeenkomst Store opgeslagen: " . $bij1k->naam);
	   $bij1k->save();
	  return View::make('evenementen.form')
		->with('bij1k', $bij1k)
		->with('action', url('evenementen/' . $bij1k->id))
		->with('success', 'Bijeenkomst ' . $bij1k->naam . ' opgeslagen');
		$bij1k->save();
		return redirect()->action('BijeenkomstController@edit', ['id' => $bij1k->id ] )->with('success' , 'Bijeenkomst "' . $bij1k->naam  . ' opgeslagen');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
  		Log::info(Auth::user()->naam . ' B16 BijeenkomstController show ' . $id);
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $exportoutput = '', $scroll = null, $vorigeTI = 0)
    {
		Log::info(Auth::user()->naam . ' B21 BijeenkomstController edit ' . $id);
		return View::make('evenementen.form')
			->with('bij1k', Bijeenkomst::find($id))
			->with('vorigeTypeId', $vorigeTI)
			->with('exportoutput', $exportoutput)
			->with('scroll', $scroll)
			->with('action', url('evenementen/' . $id));
    } 


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		Log::info(Auth::user()->naam . ' B26 BijeenkomstController update ' . $id);
       $bij1k = Bijeenkomst::find($id);
       $bij1k->naam = substr($request->naam,0,49);
       $bij1k->tijdstip = $request->datum . " " . $request->tijd;
       $bij1k->eindtijd = $request->einddatum . " " . $request->eindtijd;
       $bij1k->opmerkingen = substr($request->opmerkingen,0,499);
       if (isset($request->typeanders)) $bij1k->type = substr($request->typeanders,0,39);
		else if (isset($request->type)) $bij1k->type = $request->type;
			 else $bij1k->type = 'geen';
		$foutmeldingen = [
			'required' => 'Naam van de bijeenkomst moet ingevuld zijn.'];
		$regels = [
			'naam' => 'required'
		];
		$validator = Validator::make($request->all(), $regels, $foutmeldingen);
		if ($validator->fails()) {
			$errors = $validator->errors();
			Log::info(Auth::user()->naam . ' B28 BijeenkomstController edit validatiefout ' . $id);
            return back()->withErrors($validator)
                        ->with('bij1k', $bij1k)
						->with('action', url('evenementen'));
        }
		Log::info("B17 Bijeenkomst Edit opgeslagen: " . $bij1k->naam);
		$bij1k->save();
		return View::make('evenementen.form')
		->with('bij1k', $bij1k)
		->with('action', url('evenementen/' . $bij1k->id))
		->with('success', 'Bijeenkomst ' . $bij1k->naam . ' opgeslagen');
    }

    public function destroy($id)
    {
		Log::info(Auth::user()->naam . ' B51 Bijeenkomst ' . $id . ' verwijderen');
        $bij1k = Bijeenkomst::find($id);
        $bij1k->delete();
        $deelnames = Deelname::where('bijeenkomst_id', $id)->get();
        foreach ($deelnames as $deelname)
        {
			$deelname->delete();
		}
        return redirect()->action('BijeenkomstController@index')->with('success', 'Bijeenkomst "' . $bij1k->naam . '" verwijderd');   
    }
    
    public function deelname_toevoegen(Request $request)
    {
		Log::info(Auth::user()->naam . ' B61 Deelname  ' . $request->contact_id . ' toevoegen aan bijeenkomst ' . $request->bijeenkomst_id);
		if ($request->contact_id)
		{
			$deelname = new Deelname();
			$deelname->contact_id = $request->contact_id;
			$deelname->bijeenkomst_id = $request->bijeenkomst_id;
			$deelname->type_id = $request->type_id;
			$deelname->datum = $request->datum;
			$deelname->opmerkingen = substr($request->opmerkingen,0,499);
			$deelname->save();
			Log::info(Auth::user()->naam . " B52 Deelname opgeslagen met type " . $request->type_id);
			return redirect()->action('BijeenkomstController@edit', ['id' => $deelname->bijeenkomst_id ] )
				->with('scroll', 'betrokken')
				->with('success' , 'Deelnemer toegevoegd');
		}
		return redirect()->action('BijeenkomstController@edit', ['id' => $request->bijeenkomst_id ] );
	}

    public function deelname_verwijderen($id)
    {
		$deelname = Deelname::find($id);
		Log::info(Auth::user()->naam . ' B63 Deelname  ' . $deelname->deelnemer() . ' verwijderen van bijeenkomst ' . $deelname->bijeenkomst());
		$bijeenkomst_id = $deelname->bijeenkomst_id;
		$deelname->delete();
		Log::info("B57 Deelname $id verwijderd");
		return redirect()->action('BijeenkomstController@edit', ['id' => $bijeenkomst_id ] )
			->with('scroll', 'betrokken')
			->with('success' , 'Deelnemer verwijderd');
	}
	
	public function exporteerEmail(Request $request, $id)
	{
		$exportoutput = "";
		if (isset($request->scheiden[0]) && $request->scheiden[0] == 1)
			$scheidingsteken = ',';
		else if (isset($request->scheiden[0]) && $request->scheiden[0] == 2)
			$scheidingsteken = ';';
		else $scheidingsteken = ' ';
		if (isset($request->type)) {
			foreach ($request->type as $type)
			{
				$myType = DB::table('la_deelnametypes')->where('id', $type)->first();
				$exportoutput .= "\n\n         " . $myType->type . ":\n";
				$deelname = Deelname::where('bijeenkomst_id', $id)->where('type_id', $type)->orderBy('type_id')->get();
				$myEmails = array();
				foreach ($deelname as $deelnemer)
				{
					$contact = Contact::find($deelnemer->contact_id);
					if (isset($contact->email)) $myEmails[] = $contact->email;
				}
				$exportoutput .= implode($scheidingsteken . "\n", $myEmails);
			}
		}
		return View::make('evenementen.form')
			->with('bij1k', Bijeenkomst::find($id))
			->with('vorigeCheckboxes', $request->type)
			->with('action', url('evenementen/' . $id))
			->with('scroll', 'export')
			->with('scheiden', $request->scheiden[0])
			->with('exportoutput', $exportoutput);

	}
}
