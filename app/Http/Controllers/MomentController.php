<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Contact;
use App\Moment;
use DateTime;

class MomentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $moment = new Moment;
       	$this->vul($request, $moment);
       	$moment->klaar = false;
		$moment->save();
		$contact = Contact::find($moment->contact_id);
		return redirect()->action('ContactController@edit', ['id' => $moment->contact_id ] )->with('success' , 'Contact met ' . $contact->voornaam . " " . $contact->tussenvoegsel . " " . $contact->achternaam . ' ingevoerd');   

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       	$moment = Moment::find($id);
       	$this->vul($request, $moment);
       	$moment->klaar = ($request->klaar == 'on');
		$moment->save();
		$contact_id = $moment->contact_id;
        return redirect()->action('ContactController@edit',[ 'contact' => $contact_id ])->with('success', 'Contactmoment gewijzigd');

    }

    public function destroy($id)
    {
        $contact = Contact::find($id);
        $deelnemer = Deelnemer::find($contact->deelnemer_id);
        $contact->delete();
        return redirect()->action('DeelnemerController@index')->with('success', 'Contact met ' . $deelnemer->voornaam . " " . $deelnemer->achternaam . ' verwijderd');   
    }
    
    public function vul($request, &$mymoment)
    {
		$mymoment->contact_id = $request->contact_id;
		$mymoment->datum = $request->datum;
		$mymoment->tekst = substr($request->tekst,0,65533);
		$mymoment->type = $request->type;
		$mymoment->gebruiker_id = Auth::user()->id;
		if (isset($request->doorschuif) && $request->doorschuif != date('Y-m-d')) 
		{
			$mymoment->periode = 1; // specifieke datum
			$mymoment->doorschuif = $request->doorschuif;
		} else {
			$vandaag = new DateTime();
			if (isset($request->periode))
			{
				$mymoment->periode = $request->periode;	
				$target = $vandaag;
				switch ($request->periode)
				{
					case 1: // morgen
						$target->modify('+1 day');
						break;
					case 2: // volgende week
						$target->modify('Monday next week');
						break;
					case 3: // volgende maand
						$target->modify('first day of next month');
						break;
				}
				$mymoment->doorschuif = $target->format('Y-m-d');
			}
		}
	}

}
