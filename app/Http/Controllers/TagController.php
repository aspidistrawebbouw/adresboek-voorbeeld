<?php

namespace App\Http\Controllers;
use App\Tag;
use App\TagTekst;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
		$tags = TagTekst::orderBy('tekst')->get();
        return View::make('tags.index')
            ->with('tags', $tags);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$oudeTag = TagTekst::where('tekst', substr($request->nieuweTag, 0, 39))->first();
		if (isset($oudeTag->id))
		{
			return redirect()->action('TagController@index')
			->with('error', "Tag bestaat al");
		}

		$nieuweTag = new TagTekst();
		$nieuweTag->tekst = substr($request->nieuweTag, 0, 39);
		$nieuweTag->save();
		return redirect()->action('TagController@index')
			->with('success', "Tag opgeslagen");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$oudeTag = TagTekst::find($id);
		$oudeTag->delete();
		$contactTags = Tag::where('tag_id', $id)->get();
		if ($contactTags)
		{
			foreach ($contactTags as $ct)
			{
				$ct->delete();
			}
		}
		return redirect()->action('TagController@index')
			->with('success', "Tag verwijderd");

    }

	public function verwijdertag($id)
	{
		$tag = Tag::find($id);
		$tag->delete();
		return "OK";
	}
	
	public function wijzig(Request $request)
	{
		Log::info('T51 wijzig tagtekst nr. ' . $request->tag);
		$tag = TagTekst::find($request->tag);
		$nieuwekleur = 'kleur' . $request->tag;
		$tag->kleur = $request->$nieuwekleur;
		$tag->tekst = $request->tekst;
		$tag->save();
		return redirect()->action('TagController@index')
			->with('success', "Tag gewijzigd");
	}
	
}
