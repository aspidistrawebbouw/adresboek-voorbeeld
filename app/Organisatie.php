<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Organisatie extends Model
{
    protected $fillable = ['naam', 'plaats' , 'telnr' , 'email', 'website', 'opmerkingen'];
    protected $table = 'la_organisaties';

	public function contacten() 
	{
		return $this->hasMany('App\Contact');
	}
	
}

/* 
 *CREATE TABLE `la_deelname` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  bijeenkomst_id integer,
  contact_id integer,
  type_id integer,
  opmerkingen varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

*/
