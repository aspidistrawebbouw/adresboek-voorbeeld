<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TagTekst extends Model
{
    protected $fillable = ['tekst', 'kleur'];
    protected $table = 'la_tagteksten';
	public $timestamps = false;
	
	public function kleur() 
	{
		return DB::table('la_tagkleuren')->find($this->kleur)->kleur;
	}
}
