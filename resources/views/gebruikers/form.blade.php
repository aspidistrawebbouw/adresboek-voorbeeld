@extends('layouts.app')
@section('title', 'Details gebruiker')
@section('content')
<h3>Details @if (isset($gebruiker)) {{ $gebruiker->naam }} @else gebruiker @endif</h3>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
</div>
<form action="{{ $action }}" method="POST"  enctype="multipart/form-data">
@csrf
<?php 
use App\Contact;
use App\Deelname;
use App\Moment;
use App\User;
setlocale(LC_ALL, 'nl_NL.utf8');
	if (isset($gebruiker->id) && $gebruiker->id != 0)
	{ ?>
		<input type="hidden" name="_method" value="PUT">
<?php } else 
	{
		if (!isset($gebruiker->id))
		{
			$gebruiker = new User();
		}
	} ?>
<style>fieldset label {width: 115px;} </style>
<fieldset>
	@if (isset($gebruiker->id))
		<div class="infoblokje">
			<p><strong>Info</strong></p>
			<p>Laatst ingelogd: @if (null == $gebruiker->last_login_at || $gebruiker->last_login_at == "0000-00-00 00:00:00") Nooit @else {{ strftime('%d %B %Y %H:%M',strtotime($gebruiker->last_login_at)) }}@endif</p>
		</div>
	@endif
	<p>Velden met * zijn verplichte velden.</p>
	<p>
		<label for="naam">Naam*:</label> <input  class="focushier" type='text' id='naam' name='naam' value='{{$gebruiker->naam}}'></input>
	</p>
	<p>
		<label for="email">Email-adres*:</label> <input type='email' id='email' name='email' value='{{ $gebruiker->email }}' size="50"></input>
	</p>
	<button class="btn btn-primary waarsch">Niet gewijzigd</button>
</fieldset>
</form>
@if (isset($gebruiker->id))
<form id="verwijderen" method="POST" action = "{{ url('gebruikers/' . $gebruiker->id) }}">
	@csrf
	@method('DELETE')
	<button class="onzichtbaar btn"
		teverwijderen="{{ $gebruiker->naam }}"
		>Gebruiker {{ $gebruiker->voornaam }} verwijderen</button>
</form>
<br/>
@endif
<p><a href="{{ url('gebruikers') }}"><button class='btn btn-normaal'>&larr;Naar gebruikers</button></a>
<a href="{{ url('gebruikers/create') }}"><button class='btn btn-normaal'>Nieuwe gebruiker</button></a></p>
@endsection
@section('scripts')
	<script src="{{ url('/js/app.js') }}"></script>
	<script src="{{ url('/js/jquery.min.js') }}"></script>
	<script src="{{ url('/js/ajaxfuncties.js') }}"></script>
@endsection
