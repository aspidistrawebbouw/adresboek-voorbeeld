<?php 
use App\User;
?>
@extends('layouts.app')
@section('title', 'Gebruikers')

@section('content')

<style>
	#gebruikers {table-layout: fixed;}
	#gebruikers td, #gebruikers th {
		  white-space: normal;
		  text-overflow: ellipsis;
		  overflow: hidden;
	}

</style>
<h2>Gebruikers</h2>
<div id="app">
	@include('flash-message')
	@yield('content')
</div>
<a href="{{ url('gebruikers/create') }}"><button class='btn btn-normaal'>Nieuwe gebruiker</button></a>
<hr>
<table class="table table-striped table-bordered display compacts" style="width: 50%;" id="gebruikers">
    <thead>
        <tr>
			<th>Naam</th>
			<th>Email</th>
			<th>Laatste keer ingelogd</th>
        </tr>
    </thead>
    <tbody>
	<?php 
		setlocale(LC_ALL,'nl_NL.utf8');
		foreach ($gebruikers as $gebruiker) {
			?>
       <tr>
			<td style="white-space: nowrap; font-weight: bold;"><a href="{{ url('gebruikers/' . $gebruiker->id . '/edit') }}">{{ $gebruiker->naam}}</a></td>
			<td>{{ $gebruiker->email }}</td>
            <td>@if (null == $gebruiker->last_login_at || $gebruiker->last_login_at == "0000-00-00 00:00:00") Nooit @else {{ strftime('%d %B %Y %H:%M',strtotime($gebruiker->last_login_at)) }}@endif</td>
        </tr>
	<?php } ?>
    </tbody>
</table>
@endsection
@section('scripts')
	<script src="{{ url('/js/app.js') }}"></script>
	<script src="{{ url('/js/jquery.min.js') }}"></script>
	<script src="{{ url('/js/jquery-ui.min.js') }}"></script>
	<script src="{{ url('/js/ajaxfuncties.js') }}"></script>
@endsection
