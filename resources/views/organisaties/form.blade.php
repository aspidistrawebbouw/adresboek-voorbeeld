@extends('layouts.app')
@section('title', 'Details organisatie')
@section('content')
<h3>Details @if (isset($organisatie)) {{ $organisatie->naam }} @else organisatie @endif</h3>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
</div>
<form action="{{ $action }}" method="POST"  enctype="multipart/form-data">
@csrf
<?php 
use App\Organisatie;
use App\Contact;
setlocale(LC_ALL, 'nl_NL.utf8');
	if (isset($organisatie->id) && $organisatie->id != 0)
	{ ?>
		<input type="hidden" name="_method" value="PUT">
<?php } else 
	{
		if (!isset($organisatie->id))
		{
			$organisatie = new Organisatie();
		}
	} ?>
<style>fieldset label {width: 115px;} </style>
<fieldset>
	<p>Velden met * zijn verplichte velden.</p>
	<p>
		<label for="naam">Naam*:</label> <input class="focushier" type='text' id='naam' name='naam' value='{{$organisatie->naam}}' required size="60"></input>
	</p>
	<p>
		<label for="plaats">Plaats:</label> <input type='text' id='plaats' name='plaats' value='{{$organisatie->plaats}}'></input>
	</p>
	<p>
		<label for="email">Email-adres:</label> <input type='email' id='email' name='email' value='{{ $organisatie->email }}' size="50"></input>
	</p>
	<p>
		<label for="email">Telefoon:</label> <input type='text' id='telnr' name='telnr' value='{{ $organisatie->telnr }}' size="50"></input>
	</p>
	<p>
		<label for="website">Website:</label> <input type='text' id='website' name='website' value='{{ $organisatie->website }}' size="50"></input>
	</p>
	<p>
		<label for="opmerkingen">Opmerkingen:</label> <textarea rows="8" cols="80" id='opmerkingen' name='opmerkingen'>{{ $organisatie->opmerkingen }}</textarea>
	</p>
	<button class="btn btn-primary waarsch">Niet gewijzigd</button>
	@if (isset($organisatie->id))
		<p>Bij {{ $organisatie->naam }} kennen we deze contactpersonen:</p><p>
		<?php $orgconts = Contact::where('organisatie', $organisatie->naam)->orderBy('voornaam')->orderBy('achternaam')->get(); ?>
		@foreach ($orgconts as $orgcont)
		<a href="{{ url('contacten/' . $orgcont->id . '/edit') }}"><strong>{{ $orgcont->helenaam() }}</strong></a><br/>
		@endforeach
		</p>
	@endif
</fieldset>
</form>
@if (isset($organisatie->id))
<form id="verwijderen" method="POST" action = "{{ url('organisaties/' . $organisatie->id) }}">
	@csrf
	@method('DELETE')
	<button class="onzichtbaar btn"
		teverwijderen="{{ $organisatie->naam }}"
		>organisatie {{ $organisatie->naam }} verwijderen</button>
</form>
<br/>
@endif
<p><a href="{{ url('organisaties') }}"><button class='btn btn-normaal'>&larr;Naar organisaties</button></a>
<a href="{{ url('organisaties/create') }}"><button class='btn btn-normaal'>Nieuwe organisatie</button></a></p>
@endsection
@section('scripts')
	<script src="{{ url('/js/app.js') }}"></script>
	<script src="{{ url('/js/jquery.min.js') }}"></script>
	<script src="{{ url('/js/ajaxfuncties.js') }}"></script>
@endsection
