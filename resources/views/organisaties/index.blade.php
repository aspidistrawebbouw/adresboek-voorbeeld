<?php 
use App\Organisatie;
use App\Contact;
?>
@extends('layouts.app')
@section('title', 'Organisaties')

@section('content')

<style>
	#organisaties {table-layout: fixed; width: 100% !important;}
	#organisaties td, #organisaties th {
		  width: auto !important;
		  white-space: normal;
		  text-overflow: ellipsis;
		  overflow: hidden;
	}

</style>
<h2>Organisaties</h2>
<div id="app">
	@include('flash-message')
	@yield('content')
</div>
<a href="{{ url('organisaties/create') }}"><button class='btn btn-normaal'>Nieuwe organisatie</button></a>
<table class="table table-striped table-bordered display compacts" style="width: auto;" id="organisaties">
    <thead>
        <tr>
			<td>Naam</td>
			<td>Plaats</td>
			<td>E-mail</td>
			<td>Telnr.</td>
			<td>Website</td>
			<td>Contact-<br/>personen</td>
			<td>Opmerkingen</td>
        </tr>
    </thead>
    <tbody>
	<?php 
		foreach ($organisaties as $organisatie) {
			$count = Contact::where('organisatie',$organisatie->naam)->count();
			?>
       <tr>
			<td style="font-weight: bold; white-space: nowrap;"><a href="{{ url('/organisaties/' . $organisatie->id . '/edit') }}">{{ $organisatie->naam}}</a></td>
			<td>{{ $organisatie->plaats }}</td>
			<td><a href="mailto:{{ $organisatie->email }}">{{ $organisatie->email }}</a></td>
			<td style="white-space: nowrap;">{{ $organisatie->telnr }}</td>
			<td><a href="http://{{ $organisatie->website}}" target="_blank">{{ $organisatie->website }}</a></td></td>
			<td style="text-align: center;">{{ $count }}</td>
              <td><div style="max-height: 80px; overflow: hidden; text-overflow: ellipsis;">{{ $organisatie->opmerkingen }}</div></td>
       </tr>
	<?php } ?>
    </tbody>
</table>


@endsection
@section('scripts')
	<script src="{{ url('/js/app.js') }}"></script>
	<script src="{{ url('/js/jquery.min.js') }}"></script>
	<script src="{{ url('/js/jquery-ui.min.js') }}"></script>
	<script src="{{ url('/js/datatables.min.js') }}"></script>
	<script src="{{ url('/js/ajaxfuncties.js') }}"></script>
	<script>
$(function() {
	$('#organisaties').DataTable({'info': false, 'paging': false,language: {
        search: "Zoek in de lijst:",
    }, 'order': [[ 0, "asc" ]]
    });
 });

</script>
@endsection
