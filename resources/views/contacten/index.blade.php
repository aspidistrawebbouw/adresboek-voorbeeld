<?php 
use App\Contact;
use App\Organisatie;
?>
@extends('layouts.app')
@section('title', 'Contactpersonen')

@section('content')

<style>
	#contacten {table-layout: fixed; width: 100% !important;}
	#contacten td, #contacten th {
		  white-space: normal;
		  text-overflow: ellipsis;
	}

</style>
<h2>Contacten</h2>
<div id="app">
	@include('flash-message')
	@yield('content')
</div>
<p>Klik op de achternaam van een contactpersoon om diens gegevens te bewerken.<br/>Klik op de kolomkoppen om te sorteren.<br/>Om snel te zoeken naar een zoekwoord in een willekeurige kolom,
typ de eerste letters in "Zoek in de lijst".</p>
<a href="{{ url('contacten/create') }}"><button class='btn btn-normaal'>Nieuwe contactpersoon</button></a>
<fieldset id="filters">
	<h4>Filters</h4>
	<table>
	<tr style="vertical-align: top;">
		<td style="padding-right: 20px;">Bron:</td>
		<td style="padding-right: 20px;"><button class="filter bronfilter allemaal" rubriek="la">Allemaal</button>
		<button class="filter bronfilter geen" style="background-color: #d05050;" rubriek="la">Geen</button></td>
		<td style="padding-right: 20px;">
		<?php 
		$i = 0;
		$bronnen = DB::table('la_contacten')->select('bron')->distinct()->get();
		foreach ($bronnen as $bron)
		{ $i++; ?>
		<button class="filter bronfilter" rubriek="la">{{ $bron->bron }}</button>
		@if ($i % 6 == 0) <br/> @endif
		<?php } ?>
		</td>
	</tr>
	</table>
<p><span id="counter"></span> personen</p>
</fieldset>
<hr>
<table class="table table-striped table-bordered display compacts" style="width: auto;" id="contacten">
    <thead>
        <tr>
			<td>achternaam</td>
			<td>Voornaam</td>
			<td>Achternaam</td>
			<td>Functie/<br/>organisatie</td>
			<td>E-mail</td>
			<td>Telnr.</td>
            <td>Locatie</td>
            <td>Bron</td>
            <td>Tags</td>
            <td>Laatste contact</td>
            <td>Opmerkingen</td>
            <td>herkomst</td>
        </tr>
    </thead>
    <tbody>
	<?php 
		foreach ($contacten as $contact) {
		$dezeorg = Organisatie::where('naam',$contact->organisatie)->first(); 
			?>
       <tr>
			<td>{{ $contact->achternaam}}</td>
            <td style="white-space: nowrap; font-weight: bold;">{{ $contact->voornaam }}</td>
            <td style="font-weight: bold;"><a href="{{ URL::to('contacten/' . $contact->id . '/edit') }}">{{ $contact->tussenvoegsel }} {{ $contact->achternaam }}</a></td>
			<td style="overflow: hidden;">@if ($contact->functie){{$contact->functie}}<br/>@endif{{ $contact->organisatie }}</td>
		<?php $myemail = $contact->email; if (!isset($contact->email) && $contact->email != "" && isset($dezeorg->email) && $dezeorg->email != "") $myemail = $dezeorg->email; ?>
			<td style="overflow: hidden;"><a href="mailto:{{ $myemail }}">{{ $myemail }}</a></td>
		<?php $mytelnr = $contact->telnr; 
				if ((!isset($contact->telnr) || $contact->telnr == "" ) 
						&& isset($dezeorg->telnr) 
						&& $dezeorg->telnr != "") 
					$mytelnr = $dezeorg->telnr; 
			?>
			<td style="white-space: nowrap;">{{ $mytelnr }}</td>
		<?php $myplaats = $contact->locatie; if ((!isset($contact->locatie) || null == $contact->locatie || $contact->locatie == "") && isset($dezeorg->plaats) && $dezeorg->plaats != "") $myplaats = $dezeorg->plaats; ?>
			<td>{{ $myplaats }}</td>
			<td class="bron">{{ $contact->bron }}</td>
            <td style="line-height: 200%;"><div style="width: 250px; overflow: show;">
				@foreach ($contact->tags() as $tag)
					<span class="tag" style="background-color: {{ $tag->kleur() }};">{{ $tag->tagtekst() }}&nbsp;
						<span class="tagbutton" url="{{ url('contacten/' . $tag->id . '/verwijdertag') }}">&#x274E;</span>
					</span>
					<br/>
				@endforeach</div></td>
			<td style="text-align: center;">@if($contact->momenten()->count() > 0)<a style="font-size: 1.5em;" href="{{ url('/contacten/' . $contact->id . '/edit#cm') }}">&#9993;</a>
				<br/>
				<span style="white-space: nowrap;">{{ $contact->laatsteContactMoment() }}</span>@endif</td>
             <td><div style="max-height: 80px; overflow: hidden; text-overflow: ellipsis;">{{ $contact->opmerkingen }}</div></td>
             <td>{{ $contact->herkomst }}</td>
        </tr>
	<?php } ?>
    </tbody>
</table>


@endsection
@section('scripts')
	<script src="{{ url('/js/app.js') }}"></script>
	<script src="{{ url('/js/jquery.min.js') }}"></script>
	<script src="{{ url('/js/jquery-ui.min.js') }}"></script>
	<script src="{{ url('/js/datatables.min.js') }}"></script>
	<script src="{{ url('/js/js.cookie.js') }}"></script>
	<script>
$(function() {
	$('#contacten').DataTable({'info': false, 'paging': false,language: {
        search: "Zoek in de lijst:",
    }, 'order': [[ 0, "asc" ]],
        "columnDefs": [
		{ visible: false, targets: [ 0,11 ]}
	],'AutoWidth': true 
	    });
 });
</script>
	<script src="{{ url('/js/ajaxfuncties.js') }}"></script>
@endsection
