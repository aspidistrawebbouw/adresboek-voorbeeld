@extends('layouts.app')
@section('title', 'Details contactpersoon')
@section('content')
<p>Spring naar: <a href="#cm">Contactmomenten</a> | <a href="#bk">Bijeenkomsten</a></p>
<h3>Details @if (isset($contact)) {{ $contact->voornaam }} {{ $contact->tussenvoegsel }} {{ $contact->achternaam }}@else contactpersoon @endif</h3>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
</div>
<form action="{{ $action }}" method="POST"  enctype="multipart/form-data">
@csrf
<?php 
use Illuminate\Support\Facades\DB;
use App\Contact;
use App\Deelname;
use App\Bijeenkomst;
use App\Organisatie;
use App\Moment;
use App\User;
setlocale(LC_ALL, 'nl_NL.utf8');
	if (isset($contact->id) && $contact->id != 0)
	{ ?>
		<input type="hidden" name="_method" value="PUT">
<?php } else 
	{
		if (!isset($contact->id))
		{
			$contact = new contact();
		}
	} ?>
<style>fieldset label {width: 115px;} </style>
<fieldset>
	<p>Velden met * zijn verplichte velden.</p>
	<p>
		<label for="voornaam">Voornaam:</label> <input  class="focushier" type='text' id='voornaam' name='voornaam' value='{{$contact->voornaam}}'></input>
		<label for="tussenvoegsel">Tussenvoegsel:</label> <input type='text' id='tussenvoegsel' name='tussenvoegsel' value='{{$contact->tussenvoegsel }}' size="8"></input>
		<label for="achternaam">Achternaam*:</label> <input type='text' id='achternaam' name='achternaam' value='{{$contact->achternaam }}' required size="30"></input>
	</p>
	<p>
		<label for="functie">Functie:</label> <input type='text' id='functie' name='functie' value='{{$contact->functie}}' size="40"></input>
	</p>
	
	<p>
		<label for="locatie">Locatie:</label> <input type='text' id='locatie' name='locatie' value='{{$contact->locatie }}'  size="60"></input>
	<?php $dezeorg = Organisatie::where('naam',$contact->organisatie)->first(); ?>
	</p>
	@if (isset($contact->id))
		<div class="infoblokje">
			<p><strong>Info</strong></p>
			<p>Laatst gewijzigd: {{ $contact->updated_at }} @if($contact->laatstgewijzigd) door {{ User::find($contact->laatstgewijzigd)->naam }}@endif<br/>
			   In database sinds: {{ $contact->created_at }}<br/>
			   Gegevens afkomstig van: {{ $contact->bron }}</p>
		</div>
	@endif
	<p>
		<label for="organisatie">Organisatie:</label> <input type='text' id='organisatie' name='organisatie' value='{{$contact->organisatie }}'  size="60"></input>
	@if (isset($contact->organisatie) && isset($dezeorg->id))
		of <a href="{{ url('/organisaties/' . $dezeorg->id . '/edit') }}">ga naar deze organisatie</a></p>
		@if (isset($dezeorg->plaats) && $dezeorg->plaats != "")<span style="margin-left: 115px;">Plaats: {{ $dezeorg->plaats}}</span><br/>@endif
		@if (isset($dezeorg->email) && $dezeorg->email != "")<span style="margin-left: 115px;">Email: <a href="mailto:{{ $dezeorg->email}}">{{ $dezeorg->email}}</a></span><br/>@endif
		@if (isset($dezeorg->telnr) && $dezeorg->telnr != "")<span style="margin-left: 115px;">Tel.: {{ $dezeorg->telnr}}</span><br/>@endif
		@if (isset($dezeorg->website) && $dezeorg->website != "")<span style="margin-left: 115px;">Website: <a href="http://{{ $dezeorg->website}}" target="_blank">{{ $dezeorg->website}}</a></span><br/>@endif
	@endif
	</p>
	<p>
		<label for="email">Email-adres:</label> <input type='email' id='email' name='email' value='{{$contact->email }}' size="50"></input>
	</p>
	<p>
		<label for="telnr">Telnr.:</label> <input type='tel' id='telnr' name='telnr' value='{{$contact->telnr }}'></input>
	</p>
	<p>
		<label for="opmerkingen">Opmerkingen:</label> <textarea rows="8" cols="80" id='opmerkingen' name='opmerkingen'>{{$contact->opmerkingen }}</textarea>
	</p>
	<p>
		<label for="herkomst">Herkomst:</label>
			<select id="herkomst" name="herkomst">
				<option value="" disabled @if (!isset($contact->herkomst)) selected @endif>Kies:</option>
				<?php $herkomsten = DB::table('la_contacten')->select('herkomst')->whereNotNull('herkomst')->distinct()->orderBy('herkomst')->get(); ?>
					<option value="(geen)"@if (!$contact->herkomst) selected @endif>(geen)</option>
				@foreach ($herkomsten as $herkomst)
					<option value="{{ $herkomst->herkomst }}" @if ($contact->herkomst == $herkomst->herkomst) selected @endif>{{ $herkomst->herkomst}}</option>
				@endforeach
				</select>
		&nbsp;&nbsp;<label for="herkomstanders">anders, nl:</label>
		<input id="herkomstanders" type="text" size="50" name="herkomstanders" value=""></input>
	</p>
	<p>
		<label for="geboortedatum">Geboortedatum:</label>
		<input id="geboortedatum" type="date" name="geboortedatum" value="{{ $contact->geboortedatum }}"></input>
	</p>
	<p>
		<label for="datum_bestelling">Datum bestelling:</label>
		<input id="datum_bestelling" type="date" name="datum_bestelling" value="{{ $contact->datum_bestelling }}"></input>
	</p>
	<p><label for="mytags">Tags:</label><span id="mytags">
	@if (is_a($contact, 'App\Contact'))
		@foreach ($contact->tags() as $tag)
			<span class="tag" style="background-color: {{ $tag->kleur() }};">{{ $tag->tagtekst() }}&nbsp;
				<span class="tagbutton" url="{{ url('contacten/' . $tag->id . '/verwijdertag') }}">&#x274E;</span>
			</span>&nbsp;
		@endforeach
	@endif
	</span></p>
	<p>Bestaande tag toevoegen:&nbsp;&nbsp;
		<select name="nieuweBestaandeTag">
			<option selected disabled>Kies:</option>
		<?php
			$gebruikteTags = array();
			if (is_a($contact,'App\Contact'))
				foreach($contact->tags() as $gebruikteTag)
					$gebruikteTags[] = $gebruikteTag->tag_id;
			foreach (DB::table('la_tagteksten')->orderBy('tekst')->get() as $aot)
			{ if (!in_array($aot->id,$gebruikteTags)) 
				{ ?>
				<option value="{{ $aot->id }}">{{ $aot->tekst }}</option>
	<?php 		}
			} ?>	
		</select>
	</p>
	<p>Of maak een nieuwe tag aan:&nbsp;&nbsp; <input name="nieuweTag" size="30"></input></p>
<button class="btn btn-primary waarsch">Niet gewijzigd</button>

</fieldset>
</form>
@if(is_a($contact,'App\Contact'))
@if (isset($contact->id))
<form id="verwijderen" method="POST" action = "{{ url('contacten/' . $contact->id) }}">
	@csrf
	@method('DELETE')
	<button class="onzichtbaar btn"
		teverwijderen="{{ $contact->voornaam }} {{ $contact->tussenvoegsel }} {{ $contact->achternaam }}"
		>{{ $contact->voornaam }} {{ $contact->tussenvoegsel }} {{ $contact->achternaam }} verwijderen</button>
</form>
<br/>
@endif

<p><a href="{{ url('contacten') }}"><button class='btn btn-normaal'>&larr;Naar contacten</button></a>&nbsp;<a href="{{ url('contacten/create') }}"><button class='btn btn-normaal'>Nieuwe contactpersoon</button></a></p>
@if (isset($contact->id))
<h3 id="cm">Contact met {{ $contact->voornaam }} {{ $contact->tussenvoegsel }} {{ $contact->achternaam }}</h3>
<?php $momenten = Moment::where('contact_id', $contact->id)->orderBy('datum', 'desc')->get(); ?>
<fieldset>
	<form action="{{ url('/momenten') }}" method="POST">
	@csrf
	<p><strong>Nieuw contactmoment</strong></p>
    <div class="form-group">
		<label for="datum">Datum</label>
        <input id="datum" type="date" name="datum" value="{{ date('Y-m-d') }}"/>
        <br/>
		<label for="tekst" style="vertical-align: top;">Tekst</label>
        <textarea id="tekst" name="tekst" rows="5" cols="80"></textarea>
        <input type="hidden" name="contact_id" value="{{ $contact->id }}" />
        <br/>
		<label for="type">Soort contact:</label>
			<select id="type" name="type">
				<option value="" disabled selected>Kies:</option>
				<?php $types = DB::table('la_contacttypes')->orderBy('id')->get();
				foreach ($types as $type) 
					{ ?>
					<option value="{{ $type->id }}">{{ $type->type }}</option>
				<?php } ?>
			</select>
		<label for="typeanders">&nbsp;anders, nl:</label>
		<input id="typeanders" type="text" size="50" name="typeanders"></input>
        <br/>
        <ul id="doorschuiven">
			<li>Doorschuiven naar:</li>
			<li><label for="morgen" style="white-space: nowrap;">morgen&nbsp;&nbsp;</label><input type="radio" name="periode" value="1"></input></li>
			<li><label for="volgendeweek" style="white-space: nowrap;">volgende week&nbsp;&nbsp;</label><input type="radio" name="periode" value="2"></input></li>
			<li><label for="volgendemaand" style="white-space: nowrap;;">volgende maand&nbsp;&nbsp;</label><input type="radio" name="periode" value="3"></input></li>
			<li><label for="doorschuif" style="white-space: nowrap;">specifieke datum:&nbsp;&nbsp;</label>
			<input type="date" name="doorschuif" id="doorschuif" value="{{ date('Y-m-d') }}"></input>
		</ul>
	</div>
	<button class="btn btn-primary waarsch" style="margin-bottom: 10px;">Niet gewijzigd</button>
	</form>
	<hr style="clear: both;">
	@if ($momenten->count() > 0)
	<p><strong>Eerdere contactmomenten</strong></p>
		<table id="momenten">
			<th>Datum</th>
			<th>Van</th>
			<th>Via</th>
			<th width="400px">Contact</th>
			<th>Geagendeerd voor</th>
			<th style="text-align: center;">Klaar</th>
			<th style="text-align: center;">Wijzig</th>
			<th style="text-align: center;">Verwijder</th>
		@foreach ($momenten as $moment)
			<tr class="toonmoment" id="toon{{ $moment->id }}">
				<td>{{ $moment->datum }}</td>
				<td>{{ User::find($moment->gebruiker_id)->naam }}</td>
				<td>{{ DB::table('la_contacttypes')->where('id', $moment->type)->value('type') }}</td>
				<td class="tekst" width="400px"><?= str_replace("\n", "<br/>", $moment->tekst) ?></td>
				<td>
					@if (isset($moment->doorschuif))
						@if ($moment->periode == 1) {{ strftime('%e %B', strtotime($moment->doorschuif))  }}
						@elseif ($moment->periode == 2) de week van {{ strftime('%e %B', strtotime($moment->doorschuif))  }}
						@elseif ($moment->periode == 3) {{ strftime('%B', strtotime($moment->doorschuif)) }}
						@endif
					@endif
				</td>
				<td>@if ($moment->klaar) <span class="okeevinkje">&#x2714;</span> 
					@elseif (isset($moment->doorschuif)) 
						<button class="klaarmelding btn btn-normaal" 
							url="{{ url('contacten/' . $moment->id . '/meldklaar') }}"
							moment_id="{{ $moment->id }}"><span class="okeevinkje">&#x2714;</span>Klaar</button>@endif</td>
				<td style="text-align: center;"><input type="image" class="toonwijzig" item="{{ $moment->id }}" src="{{ asset('grafisch/wijzig.png') }}" width="15px" /></td>
				<td style="text-align: center;"><a href="{{ URL::to('contacten/' . $moment->id . '/momentverwijderen') }}"><input type="image" src="{{ asset('grafisch/delete.png') }}" width="15px" /></a></td>
			</tr>
			<tr>
				<td colspan="8">
				<div id="wijzig{{ $moment->id }}"  class="wijzigmoment">
				<p><strong>Wijzig contactmoment:</strong></p>
				<form action="{{ url('momenten/' . $moment->id ) }}" method="POST">
					<input type="hidden" name="contact_id" value="{{ $contact->id }}"></input>
					@csrf
					<input type="hidden" name="_method" value="PUT">
					<div style="margin: 0 20px 5px 0;">
						Datum: <input type="date" name="datum" value="{{ $moment->datum }}"></input>
					</div>
					<textarea id="tekst" name="tekst" rows="5" cols="80">{{ $moment->tekst }}</textarea>
					<p>Soort contact:&nbsp;<select id="type" name="type">
						<?php
							$types = DB::table('la_contacttypes')->get();
							foreach ($types as $type) 
							{ ?>
								<option value="{{ $type->id }}" @if ($type->id == $moment->type_id) selected @endif>{{ $type->type }}</option>
						<?php } ?>
						</select>
					</p>
					@if (isset($moment->doorschuif))
						<p>
						Staat op de agenda voor:
						@if ($moment->periode == 1) {{ strftime('%e %B', strtotime($moment->doorschuif))  }}
						@elseif ($moment->periode == 2) de week van {{ strftime('%e %B', strtotime($moment->doorschuif))  }}
						@elseif ($moment->periode == 3) {{ strftime('%B', strtotime($moment->doorschuif)) }}
						@endif
						en is <span class="okeevinkje">&#x2714;</span>klaar: <input type="checkbox" name="klaar" @if ($moment->klaar) checked @endif></input>
						</p>
					@endif
					<ul id="doorschuiven">
						<li>Doorschuiven naar:</li>
						<li><label for="morgen" style="white-space: nowrap;">morgen&nbsp;&nbsp;</label><input type="radio" name="periode" value="1"></input></li>
						<li><label for="volgendeweek" style="white-space: nowrap;">volgende week&nbsp;&nbsp;</label><input type="radio" name="periode" value="2"></input></li>
						<li><label for="volgendemaand" style="white-space: nowrap;;">volgende maand&nbsp;&nbsp;</label><input type="radio" name="periode" value="3"></input></li>
						<li><label for="doorschuif" style="white-space: nowrap;">specifieke datum:&nbsp;&nbsp;</label>
						<input type="date" name="doorschuif" id="doorschuif" value="{{ date('Y-m-d') }}"></input>
					</ul><br/>
					
					<button class="btn btn-primary waarsch" type="submit" style="float: right;" >Opslaan</button>
					</form>
					<button class="btn btn-normaal cancel" style="float: left;" item="{{ $moment->id }}">Annuleren</button>
					</div>
				</td>
			</tr>
		@endforeach
		</table>
	@endif
</fieldset>
<h3 id="bk">Deelname aan bijeenkomsten</h3>
	<?php $deelname = Deelname::where('contact_id', $contact->id)->orderBy('datum', 'desc')->get(); ?>
<fieldset>
	@if ($deelname->count() > 0)
	<table>
		@foreach($deelname as $deelnam)
		<tr><td style="padding-right: 20px;">{{ $deelnam->datum }}</td>
			<td style="padding-right: 20px;"><a href="{{ url('evenementen/' . $deelnam->bijeenkomst_id . '/edit') }}">{{ $deelnam->bijeenkomst() }}</a></td>
			<td style="padding-right: 20px;">{{ $deelnam->type() }}</td>
			<td style="padding-right: 20px;">{{ $deelnam->opmerkingen }}</td>
			<td style="padding-right: 20px;"><a href="{{ url('evenement/verwijderen/' . $deelnam->id) }}"><img src="{{ url('grafisch/delete.png') }}" width="15px"></a></td>
		</tr>
		@endforeach
	</table>
	@else
	<p>{{ $contact->helenaam() }} heeft niet aan een bijeenkomst deelgenomen.</p>
	@endif
	
	<p><strong>{{ $contact->helenaam() }} toevoegen aan een bijeenkomst:</strong></p>
	<form action="{{ url('/evenement/toevoegen') }}" method="POST">
	@csrf
		<input type="hidden" name="contact_id" value="{{ $contact->id }}"></input>
		<label for="bijeenkomst" style="width: auto; vertical-align: baseline;">Bijeenkomst:</label>
		<select name="bijeenkomst_id" id="bijeenkomst">
			<option disabled selected>Kies:</option>
		<?php
			$bijeenkomsten = Bijeenkomst::orderBy('tijdstip')->orderBy('naam')->get();
			foreach ($bijeenkomsten as $bij1k)
			{ ?>
				<option value="{{ $bij1k->id }}">{{ strftime('%e %B %Y', strtotime($bij1k->tijdstip)) }} - {{ $bij1k->naam }}</option>
			<?php } ?>
		</select>			
		<label for="type_id" style="width: auto; vertical-align: baseline;">Wat:</label>
		<select name="type_id" id="type_id">
		<?php
			$types = DB::table('la_deelnametypes')->get();
			foreach ($types as $type)
			{ ?>
				<option value="{{ $type->id }}" @if (isset($vorigeTypeId) && $vorigeTypeId == $type->id)) selected @endif>{{ $type->type }}</option>
			<?php } ?>
		</select>
		<label for="datum" style="width: auto; vertical-align: baseline;">Datum:</label>
		<input name="datum" type="date" id="datum" value="{{ date('Y-m-d') }}"></input>
		<label for="opmerkingen" style="width: auto; vertical-align: baseline;">Opmerking:</label>
		<input name="opmerkingen" type="text" id="opmerkingen">
		<button class="btn btn-primary waarsch" type="submit">Niet gewijzigd</button>
	</form>
</fieldset>
	
<p><a href="{{ url('contacten') }}"><button class='btn btn-normaal'>&larr;Naar contacten</button></a></p>
@endif
@endif
@endsection
@section('scripts')
	<script src="{{ url('/js/app.js') }}"></script>
	<script src="{{ url('/js/js.cookie.js') }}"></script>
	<!-- script src="{{ url('/js/jquery.min.js') }}"></script -->
	<script src="{{ url('/js/ajaxfuncties.js') }}"></script>
	<script src="{{ url('/js/datatables.min.js') }}"></script>
@endsection
