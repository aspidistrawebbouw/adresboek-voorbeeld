<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ url('favicon.ico') }}" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A300%2C400%2C700%2C300italic%2C400italic%2C700italic%7CBitter%3A400%2C700&subset=latin%2Clatin-ext" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/adresboek.css') }}" rel="stylesheet">
	<!-- link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" -->
	<link rel="stylesheet" href="{{ url('/css/datatables.min.css') }}">
	
     <title>Adresboek | @yield('title')</title>



</head>
<body>
	<nav class="navbar navbar-expand-md navbar-light navbar-laravel" style="position: fixed; z-index: 151; width: 1280px;">
		<div class="container">
			<a class="navbar-brand" href="{{ url('/') }}">
				{{ config('app.name', 'Laravel') }}
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<!-- Left Side Of Navbar -->
				<ul class="navbar-nav mr-auto">
					<li><a href="{{ url('home') }}"><button class="btn">Home</button></a>&nbsp;</li>
					<li><a href="{{ url('contacten') }}"><button class="btn">Contacten</button></a>&nbsp;	</li>
					<li><a href="{{ url('evenementen') }}"><button class="btn">Bijeenkomsten</button></a>&nbsp;	</li>
					<li><a href="{{ url('organisaties') }}"><button class="btn">Organisaties</button></a>&nbsp;	</li>
					<li><a href="{{ url('tags') }}"><button class="btn">Tags</button></a>&nbsp;	</li>
					<li><a href="{{ url('gebruikers') }}"><button class="btn">Gebruikers</button></a>&nbsp;	</li>

				</ul>

				<!-- Right Side Of Navbar -->
				<ul class="navbar-nav ml-auto">
					<!-- Authentication Links -->
					@guest
						<li class="nav-item">
							<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
						</li>
					@else
						<li class="nav-item dropdown">
							<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
								Welkom, {{ Auth::user()->naam }} <span class="caret"></span>
							</a>

							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="{{ route('logout') }}"
								   onclick="event.preventDefault();
												 document.getElementById('logout-form').submit();">
									{{ __('Logout') }}
								</a>

								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</div>
						</li>
					@endguest
				</ul>
			</div>
		</div>
	</nav>
	<div class="container" style="padding-top: 100px;">
		@yield('content')
	</div>
	@show
<div class="spacer">&nbsp;</div>
<footer>
	<p style="margin-top: 20px;"><a href="https://aspidistra.nl" target="_blank">Aspidistra Webbouw</a></p>
</footer>
@yield('scripts')
</body>
</html>
