@extends('layouts.app')
@section('title', 'Details Bijeenkomst')
@section('content')
<h3>Details @if (isset($bij1k)) {{ $bij1k->naam }}@else bijeenkomst @endif</h3>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
</div>
<form action="{{ $action }}" method="POST"  enctype="multipart/form-data">
@csrf
<?php 
use Illuminate\Support\Facades\DB;
use App\Bijeenkomst;
use App\Contact;
use App\Deelname;
setlocale(LC_ALL, 'nl_NL.utf8');
	if (isset($bij1k->id) && $bij1k->id != 0)
	{ ?>
		<input type="hidden" name="_method" value="PUT">
<?php } else 
	{
		if (!isset($bij1k->id))
		{
			$bij1k = new Bijeenkomst();
		}
	} ?>
<style>fieldset label {width: 115px;} </style>
<fieldset>
	<p>Velden met * zijn verplichte velden.</p>
	<p>
		<label for="naam">Naam*:</label> <input  class="focushier" type='text' id='naam' name='naam' value='{{$bij1k->naam}}' size="50" required></input>
	</p>
	<p>
		<label for="datumtijd">Datum/tijd:</label> 
			<?php if (isset($bij1k->tijdstip) & $bij1k->tijdstip != "0000-00-00 00:00:00") 
			{
				$mydate = strtotime($bij1k->tijdstip);
				$datum = strftime("%Y-%m-%d",$mydate);
				$tijd = strftime("%H:%M", $mydate);
			} else 
			{
				$tijd = "10:00";
				$datum = date('Y-m-d');
			} ?>
			<input type="date" id="datumtijd" name="datum" value="{{ $datum }}"></input>
			<input type="time" name="tijd" min="8:00" max="22:00" value="{{ $tijd }}"></input>
		<label for="einddatumtijd">Eindtijd:</label> 
			<?php if (isset($bij1k->eindtijd) & $bij1k->eindtijd != "0000-00-00 00:00:00") 
			{
				$mydate = strtotime($bij1k->eindtijd);
				$datum = strftime("%Y-%m-%d",$mydate);
				$tijd = strftime("%H:%M", $mydate);
			} else if (isset($bij1k->tijdstip) & $bij1k->tijdstip != "0000-00-00 00:00:00")
			{
				$mydate = strtotime($bij1k->tijdstip);
				$datum = strftime("%Y-%m-%d",$mydate);
				$tijd = strftime("%H:%M", $mydate);
			} else 
			{
				$tijd = "12:00";
				$datum = date('Y-m-d');
			} ?>
			<input type="date" id="einddatumtijd" name="einddatum" value="{{ $datum }}"></input>
			<input type="time" name="eindtijd" min="8:00" max="22:00" value="{{ $tijd }}"></input>
	</p>
	<p>
		<label for="opmerkingen">Opmerkingen:</label> <textarea rows="8" cols="80" id='opmerkingen' name='opmerkingen'>{{$bij1k->opmerkingen }}</textarea>
	</p>
	<p>
		<label for="type">Type bijeenkomst:</label>
			<select id="type" name="type">
				<option value="" disabled @if (!isset($bij1k->type)) selected @endif>Kies:</option>
			<?php $types = DB::table('la_bijeenkomsten')->select('type')->distinct()->get();
				foreach($types as $type)
				{ ?>
				<option value="{{ $type->type }}" @if ($bij1k->type == $type->type) selected @endif>{{ $type->type }}</option>
			<?php } ?>
				</select>
		<label for="typeanders">anders, nl:</label>
		<input id="typeanders" type="text" size="50" name="typeanders" value="{{ $bij1k->typeanders }}"></input>
	</p>
<button class="btn btn-primary waarsch">Niet gewijzigd</button>

</fieldset>
</form>
@if (isset($bij1k->id))
<form id="verwijderen" method="POST" action = "{{ url('evenementen/' . $bij1k->id) }}">
	@csrf
	@method('DELETE')
	<button class="onzichtbaar btn"
		teverwijderen="{{ $bij1k->naam }}"
		>Bijeenkomst "{{ $bij1k->naam }}" verwijderen</button>
</form>
<br/>
<h3 id="betrokken">Wie zijn/waren erbij betrokken?</h3>
<fieldset>
	<?php 
		$deelnames = Deelname::where('bijeenkomst_id', $bij1k->id)->orderBy('type_id')->get(); ?>
		<p>In totaal {{ $deelnames->count() }} deelnemers.</p>

	<table style="width: 75%;">
		<thead>
			<th>Naam</th>
			<th>Datum</th>
			<th>Opmerkingen</th>
			<th>Wijzig</th>
			<th>Verwijder</th>
		</thead>
	<?php	
	$deelnames = Deelname::where('bijeenkomst_id', $bij1k->id)->orderBy('type_id')->get();
	$vorigeType = 0;
	foreach ($deelnames as $deelname)
	{   if ($deelname->type_id != $vorigeType) { 
			$vorigeType = $deelname->type_id;
			$type = DB::table('la_deelnametypes')->find($deelname->type_id); ?>
			<tr><td colspan="5"><strong>{{ $type->type }}:</strong></td></tr>
		<?php }
		$mycontact = Contact::find($deelname->contact_id);
		if (isset($mycontact)) { ?>
			<tr class="toonbijeenkomst" id="toon{{ $deelname->id }}">
				<td style="padding-right: 20px; font-weight: bold;"><a href="{{ url('contacten/' . $mycontact->id . '/edit') }}">{{ $mycontact->helenaam() }}</a></td>
				<td style="padding-right: 20px;">{{ $deelname->datum }}</td>
				<td style="padding-right: 20px;">{{ $deelname->opmerkingen }}</td>
				<td style="text-align: center;"><input type="image" class="toonwijzig" item="{{ $deelname->id }}" src="{{ asset('grafisch/wijzig.png') }}" width="15px" /></td>
				<td><a href="{{ url('deelname/verwijderen/' . $deelname->id) }}"><img src="{{ url('grafisch/delete.png') }}" width="15px"></a></td>
			</tr>
			<tr>
				<td colspan="5">
					<div id="wijzig{{ $deelname->id }}" class="wijzigbijeenkomst">
						<form action="{{ url('/deelname/' . $deelname->id) }}" method="POST">
						@csrf
						<input type="hidden" name="_method" value="PUT">
						<p><strong>Wijzig de deelname van {{ $mycontact->helenaam() }} aan deze bijeenkomst:</strong></p>
						<p>Datum: <input type="date" name="datum" value="{{ $deelname->datum }}"></input></p>
						<p>Opmerkingen: <input name="opmerkingen" size="50" value="{{ $deelname->opmerkingen }}"></input>
						<p>Hoe: <select name="type_id" id="type_id">
							<?php 
								$types = DB::table('la_deelnametypes')->orderBy('id')->get();
								foreach ($types as $type)
								{ ?>
									<option value="{{ $type->id }}" @if ($deelname->type_id == $type->id)) selected @endif>{{ $type->type }}</option>
							<?php } ?>
								</select>
						</p>
						<button class="btn btn-primary" type="submit" style="float: right;" >Opslaan</button>
						</form>
						<button class="btn btn-normaal cancel" style="float: left;" item="{{ $deelname->id }}">Annuleren</button>
					</div>
				</td>
				
			</tr>
	<?php	}
	} ?>
</table>
<hr/>
<p><strong>Deelnemer toevoegen:</strong></p>
	<form action="{{ url('/deelname/toevoegen') }}" method="POST">
	@csrf
		<input type="hidden" name="bijeenkomst_id" value="{{ $bij1k->id }}"></input>
		<label for="contact" style="width: auto; vertical-align: baseline;">Wie:</label>
		<select name="contact_id" id="contact">
			<option disabled selected>Kies:</option>
		<?php
			$contacten = Contact::orderBy('voornaam')->orderBy('tussenvoegsel')->orderBy('achternaam')->get();
			foreach ($contacten as $contact)
			{ ?>
				<option value="{{ $contact->id }}">{{ $contact->helenaam() }}</option>
			<?php } ?>
		</select>			
		<label for="type_id" style="width: auto; vertical-align: baseline;">Wat:</label>
		<select name="type_id" id="type_id">
		<?php
			$types = DB::table('la_deelnametypes')->orderBy('id')->get();
			foreach ($types as $type)
			{ ?>
				<option value="{{ $type->id }}" @if (isset($vorigeTypeId) && $vorigeTypeId == $type->id)) selected @endif>{{ $type->type }}</option>
			<?php } ?>
		</select>
		<label for="datum" style="width: auto; vertical-align: baseline;">Datum:</label>
		<input name="datum" type="date" id="datum" value="{{ date('Y-m-d') }}"></input>
		<label for="opmerkingen" style="width: auto; vertical-align: baseline;">Opmerking:</label>
		<input name="opmerkingen" type="text" id="opmerkingen">
		<button class="btn btn-primary" type="submit">Toevoegen</button>
	</form>
</fieldset>
<?php
		$voorkomende_types = Deelname::where('bijeenkomst_id', $bij1k->id)->pluck('type_id');
		$types = DB::table('la_deelnametypes')->whereIn('id', $voorkomende_types)->distinct()->get();
		?>
@if ($types->count() > 0)
<h3 id="export">Exporteren email-adressen</h3>
<p>Vink aan van welke categorieën deelnemers je de emailadressen wilt exporteren.</p>
<fieldset>
		<form action="{{ url('/evenement/exporteerEmail/' . $bij1k->id) }}" method="POST">
	@csrf
	@foreach ($types as $type)
			<input type="checkbox" name="type[]" value="{{ $type->id }}" 
			<?php if (!isset($vorigeCheckboxes) || in_array($type->id, $vorigeCheckboxes)) { ?> checked <?php } ?>	>{{ $type->type }}</input><br/>
	@endforeach
		<p>&nbsp;</p>
		<p>Emailadressen scheiden met:
			<input type="radio" name="scheiden[]" value="1" @if (!isset($scheiden) || $scheiden == 1) checked @endif>Komma</input>
			<input type="radio" name="scheiden[]" value="2" @if (isset($scheiden) && $scheiden == 2) checked @endif>Puntkomma</input>
			<input type="radio" name="scheiden[]" value="3" @if (isset($scheiden) && $scheiden == 3) checked @endif>Geen</input> </p>
		<button class="btn btn-primary" type="submit">Exporteer</button></a>
	</form>
	@if (isset($exportoutput))
		<hr style="clear: both;"/>
		<pre id="exportlijst">{{ $exportoutput }} </pre>
	@endif
</fieldset>
@endif

@endif

<p><a href="{{ url('evenementen') }}"><button class='btn btn-normaal'>&larr;Naar bijeenkomsten</button></a>
<a href="{{ url('evenementen/create') }}"><button class='btn btn-normaal'>Nieuwe bijeenkomst</button></a></p>
@endsection
@section('scripts')
	<script src="{{ url('/js/app.js') }}"></script>
	<script src="{{ url('/js/jquery.min.js') }}"></script>
	<script src="{{ url('/js/ajaxfuncties.js') }}"></script>
	@if (isset($scroll))
	<script>
		document.getElementById('{{ $scroll }}').scrollIntoView();
	</script>
	@endif
@endsection
