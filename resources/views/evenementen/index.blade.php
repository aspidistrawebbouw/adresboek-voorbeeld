<?php 
use App\Bijeenkomst;
?>
@extends('layouts.app')
@section('title', 'Bijeenkomsten')

@section('content')

<style>
	#bijeenkomsten {table-layout: fixed; width: 100% !important;}
	#bijeenkomsten td, #bijeenkomsten th {
		  width: auto !important;
		  white-space: normal;
		  text-overflow: ellipsis;
		  overflow: hidden;
	}

</style>
<h2>Bijeenkomsten</h2>
<div id="app">
	@include('flash-message')
	@yield('content')
</div>
<a href="{{ url('evenementen/create') }}"><button class='btn btn-normaal'>Nieuwe bijeenkomst</button></a>
<fieldset id="filters">
	<h4>Filters</h4>
	<table>
	<tr style="vertical-align: top;">
		<td style="padding-right: 20px;">Type bijeenkomst:</td>
		<td style="padding-right: 20px;"><button class="filter bronfilter allemaal" rubriek="lb">Allemaal</button>
		<button class="filter bronfilter geen" style="background-color: #d05050;" rubriek="lb">Geen</button></td>
		<td style="padding-right: 20px;">
		<?php 
		$i = 0;
		$typen = DB::table('la_bijeenkomsten')->select('type')->distinct()->get();
		foreach ($typen as $type)
		{ $i++; ?>
		<button class="filter bronfilter" rubriek="lb">{{ $type->type }}</button>
		@if ($i % 5 == 0) <br/> @endif
		<?php } ?>
		</td>
	</tr>
	</table>
</fieldset>
<hr>
<table class="table table-striped table-bordered display compacts" style="width: auto;" id="bijeenkomsten">
    <thead>
        <tr>
			<th>Naam</th>
			<th>Datum / tijd</th>
			<th>Type</th>
            <th>Opmerkingen</th>
            <th>Aantal deelnemers</th>
        </tr>
    </thead>
    <tbody>
	<?php 
		setlocale(LC_ALL,'nl_NL.utf8');
		$gepasseerd = 0;
		foreach ($bijeenkomsten as $bij1k) {
		?>
       <tr>
			<td style="white-space: nowrap; font-weight: bold;"><a href="{{ url('evenementen/' . $bij1k->id . '/edit') }}">{{ $bij1k->naam}}</a></td>
            <td data-sort="{{$bij1k->tijdstip }}">{{ strftime('%e %B %Y %H:%M',strtotime($bij1k->tijdstip)) }}
				@if (isset($bij1k->eindtijd) & $bij1k->eindtijd != "0000-00-00 00:00:00")
				-
					@if (substr($bij1k->tijdstip,0,10) == substr($bij1k->eindtijd,0,10))
						{{ strftime('%H:%M', strtotime($bij1k->eindtijd)) }}
					@else
						{{ strftime('%e %B %Y %H:%M',strtotime($bij1k->eindtijd)) }}
					@endif
				@endif            
            </td>
			<td class="bron">{{  $bij1k->type }}</td>
            <td style="max-height: 40px;">{{ substr($bij1k->opmerkingen,0,100) }}@if (strlen($bij1k->opmerkingen) > 99)...@endif</td>
            <td>
	<?php
			$deelnemersPerType = DB::table('la_deelname')
				->join('la_deelnametypes', 'la_deelname.type_id', '=', 'la_deelnametypes.id')
				->select('type_id', 'type', DB::raw('count(contact_id) as aantal'))
				->where('bijeenkomst_id', '=', $bij1k->id)
				->groupBy('type_id')
				->groupBy('type')
				->get();
			foreach($deelnemersPerType as $dlnType)
			{ ?>
				{{ $dlnType->type }}: {{ $dlnType->aantal }}<br/>
			<?php } ?>
			</td>
        </tr>
	<?php } ?>
    </tbody>
</table>


@endsection
@section('scripts')
	<script src="{{ url('/js/app.js') }}"></script>
	<script src="{{ url('/js/jquery.min.js') }}"></script>
	<script src="{{ url('/js/jquery-ui.min.js') }}"></script>
	<script src="{{ url('/js/datatables.min.js') }}"></script>
	<script src="{{ url('/js/js.cookie.js') }}"></script>
	<script>
$(function() {
	$('#bijeenkomsten').DataTable({'info': false, 'paging': false,language: {
        search: "Zoek in de lijst:",
    }, 'order': [[ 1, "asc" ]]
    });
 });

</script>
	<script src="{{ url('/js/ajaxfuncties.js') }}"></script>
@endsection
