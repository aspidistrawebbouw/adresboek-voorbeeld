<?php 
use App\Contact;
use App\Tag;
use App\TagTekst;
use App\TagKleur;
?>
@extends('layouts.app')
@section('title', 'Tags')

@section('content')

<style>

	#tags td, #tags th {
		  white-space: normal;
		  text-overflow: ellipsis;
	}

</style>
<h2>Tags</h2>
<div id="app">
	@include('flash-message')
	@yield('content')
	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<li >{{ $error }}</li>
			@endforeach
		</ul>
	@endif 
</div>
<div style="width: 75%; margin: 0 auto;"><p>Maak een nieuwe tag aan: <form action="{{ url('/tags') }}" method="POST">
	@csrf
	<input type="text" name="nieuweTag" size="40"></input>&nbsp;<button class="btn">Opslaan</button></form></p>
	</div>
	<p style="width: 75%; margin: 0 auto;">"Tag verwijderen" haalt de tag weg bij alle contactpersonen. Het verwijdert niet de contactpersonen zelf.</p>
<table class="table table-striped table-bordered display compacts" style="width: 100%; margin: 0 auto;" id="tags">
    <thead>
        <tr>
			<td>Tag</td>
			<td>Aantal</td>
			<td>Contactpersonen met deze tag</td>
			<td>Tekst</td>
			<td>Kleur</td>
			<td>Tekst/kleur wijzigen<br/></td>
			<td>Tag verwijderen</td>
        </tr>
    </thead>
    <tbody>
		@foreach ($tags as $tag)
		<?php $getagden = Tag::where('tag_id',$tag->id)->get(); ?>
       <tr>
			<td><span class="tag" style="background-color: {{ $tag->kleur() }}">{{ $tag->tekst }}</span></td>
			<td style="text-align: center;">{{ $getagden->count() }}</td>
			<td style="width: 30% !important;">
				<?php $contactLinks = array();
				foreach ($getagden as $getagd)
				{ 
					$contactLinks[] = '<a href="' . url('/contacten/' . $getagd->contact_id . '/edit') . '">' . $getagd->heleNaam() . '</a>';
				}
				echo implode(', ',$contactLinks);
				?>
			</td>
			<style>.kleurbox {width: 15px; height: 15px; display: inline-block; }</style>
			<form method="POST" action="{{ url('/tags/wijzig') }}">
					@csrf
			<td><input type="text" name="tekst" value="{{ $tag->tekst }}"></input></td>
			<td>
				@foreach (TagKleur::all() as $kleur)
				<div style="display: inline; float: left; padding-right: 12px;">
					<input type="hidden" name="tag" value="{{ $tag->id }}"></input>
					<div class="kleurbox" style="background-color: {{ $kleur->kleur }}"></div><br/>
					<input style="margin-left: 2px;" width="25px" type="radio" name="kleur{{ $tag->id }}" value="{{ $kleur->id }}" @if ($kleur->id == $tag->kleur) checked @endif></input><br/>
				</div>
				@endforeach
			</td>
			<td style="text-align: center;"><button class="btn">Wijzig</button></td>
			</form>
             <td style="text-align: center;">
				 <form action="{{ url('/tags/' . $tag->id ) }}" method="POST">
					@csrf
					<input type="hidden" name="_method" value="DELETE"></input>
					<input type="image" src="{{ asset('grafisch/delete.png') }}" width="15px">
				 </form>
			</td>
        </tr>
		@endforeach
    </tbody>
</table>
@endsection
@section('scripts')
	<script src="{{ url('/js/app.js') }}"></script>
	<script src="{{ url('/js/jquery.min.js') }}"></script>
	<script src="{{ url('/js/jquery-ui.min.js') }}"></script>
	<script src="{{ url('/js/js.cookie.js') }}"></script>
	<script src="{{ url('/js/ajaxfuncties.js') }}"></script>
@endsection
