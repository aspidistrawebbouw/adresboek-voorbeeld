@extends('layouts.app')
@section('title', 'Home')

<?php
use App\Contact;
use App\User;
use App\Moment;
use App\Bijeenkomst;
use Illuminate\Support\Facades\DB;
?>

@section('content')
<h2>Agenda</h2>
<table id="agenda" style="width: 960px;">

<?php
	$alletijdvakken = [
		['Today', 1, 'Vandaag'],
		['Tomorrow' , 1 , 'Morgen'],
		['Monday this week' , 2 , 'Deze week'],
		['Monday next week' , 2 , 'Volgende week'],
		['first day of this month' , 3 , 'Deze maand'],
		['first day of next month' , 3 , 'Volgende maand']
	] ;
	$welitems = false;
	foreach($alletijdvakken as $mijntijdvak)
	{
		$target = new DateTime(); $target->modify($mijntijdvak[0]);
		$tedoen = Moment::where('doorschuif',$target->format('Y-m-d'))->where('periode',$mijntijdvak[1])->get();
		if ($tedoen->isNotEmpty())
		{
			$welitems = true;
			echo '<tr><td colspan="2" class="agendakop">' . $mijntijdvak[2] . '</td></tr>';

			foreach ($tedoen as $ted)
			{ 
				$contact = Contact::find($ted->contact_id);
				?>
				<tr>
					<td style="width: 750px"><a href="{{ url('contacten/' . $contact->id . '/edit#cm') }}">{{ $contact->voornaam }} {{ $contact->tussenvoegsel }} {{ $contact->achternaam }}</a><br/>
					{{ $ted->tekst }}</td>
					<td>@if ($ted->klaar) <span class="okeevinkje">&#x2714;</span> 
						@else<button class="klaarmelding btn btn-normaal" 
							url="{{ url('contacten/' . $ted->id . '/meldklaar') }}"
							moment_id="{{ $ted->id }}"><span class="okeevinkje">&#x2714;</span>Klaar</button>@endif</td>
				</tr>
	<?php 	} 
		}
	}
	if (!$welitems)
	{ ?>
		<tr><td colspan="2">Geen items!</td></tr>
	<?php
	}
	?>
</table>
<hr style="margin-top: 20px; border-bottom: 1px solid orange;"/>
<h2>Logboek</h2>
<table id="logboek">
	<tr>
		<th>Naam</th>
		<th>Datum</th>
		<th>Soort contact</th>
		<th>Met</th>
		<th style="width: 500px;">Tekst</th>
		<th>Doorgeschoven</th>
	</tr>
<?php
	$recent = Moment::orderBy('datum', 'desc')->orderBy('updated_at', 'desc')->take(15)->get();
	foreach ($recent as $rec)
	{ 	setlocale(LC_ALL,'nl_NL.utf8');
		$contact = Contact::find($rec->contact_id);
		if (isset($contact))
		{
		?>
		<tr>
			<td class="nowrap"><a href="{{ url('contacten/' . $contact->id . '/edit#cm') }}">{{ $contact->voornaam }} {{ $contact->tussenvoegsel }} {{ $contact->achternaam }}</td>
			<td class="nowrap">{{ $rec->datum }}</td>
			<td>{{ DB::table('la_contacttypes')->where('id', $rec->type)->value('type') }}</td>
			<td>{{ User::find($rec->gebruiker_id)->naam }}</td>
			<td><div style="max-height: 85px; overflow: hidden;">{{ $rec->tekst }}</div></td>
			<td>@if (isset($rec->doorschuif) && $rec->doorschuif > date('Y-m-d') && !$rec->klaar)
						@if ($rec->periode == 1) {{ strftime('%e %B', strtotime($rec->doorschuif))  }}
						@elseif ($rec->periode == 2) de week van {{ strftime('%e %B', strtotime($rec->doorschuif))  }}
						@elseif ($rec->periode == 3) {{ strftime('%B', strtotime($rec->doorschuif)) }}
						@endif
				@endif</td>
		</tr>
	<?php }
	} ?>
</table>
<hr style="margin-top: 20px; border-bottom: 1px solid orange;"/>
<h2>Aankomende bijeenkomsten</h2>
<?php $bij1kn = Bijeenkomst::where('tijdstip', '>=', date('Y-m-d'))->orderBy('tijdstip')->get(); ?>
<table id="bijeenkomsten" width="75%">
@foreach ($bij1kn as $bij1k)
	<tr>
		<td><a href="{{ url('/evenementen/' . $bij1k->id . '/edit') }}">{{ $bij1k->naam }}</a></td>
		<td>{{ strftime('%e %B %Y %H:%M',strtotime($bij1k->tijdstip)) }}
				@if (isset($bij1k->eindtijd))
				-
					@if (substr($bij1k->tijdstip,0,10) == substr($bij1k->eindtijd,0,10))
						{{ strftime('%H:%M', strtotime($bij1k->eindtijd)) }}
					@else
						{{ strftime('%e %B %Y %H:%M',strtotime($bij1k->eindtijd)) }}
					@endif
				@endif     
		</td>
		<td>
			<?php
			$deelnemersPerType = DB::table('la_deelname')
				->join('la_deelnametypes', 'la_deelname.type_id', '=', 'la_deelnametypes.id')
				->select('type_id', 'type', DB::raw('count(contact_id) as aantal'))
				->where('bijeenkomst_id', '=', $bij1k->id)
				->groupBy('type_id')
				->groupBy('type')
				->get();
			$outputItems = array();
			foreach($deelnemersPerType as $dlnType) { $outputItems[] = $dlnType->type . ": " . $dlnType->aantal; }
			echo implode(", ", $outputItems);
			?>
		</td>
</td>
	</tr>
@endforeach
</table>
<hr style="margin-top: 20px; border-bottom: 1px solid orange;"/>
<h2>Recent gewijzigde contactpersonen</h2>
<table id="recent">
	<tr><th>Naam</th><th>Laatst gewijzigd</th><th>door</th></tr>
<?php
	$recent = Contact::orderBy('updated_at', 'desc')->take(5)->get();
	foreach ($recent as $rec)
	{ ?>
		<tr>
			<td><a href="{{ url('contacten/' . $rec->id . '/edit') }}">{{ $rec->voornaam }} {{ $rec->tussenvoegsel }} {{ $rec->achternaam }}</td>
			<td class="nowrap">{{ substr($rec->updated_at,0,16) }}</td>
			<td>{{ User::find($rec->laatstgewijzigd)->naam }}</td>
		</tr>
	<?php } ?>
</table>
<hr style="margin-top: 20px; border-bottom: 1px solid orange;"/>

@endsection
@section('scripts')
	<script src="{{ url('/js/app.js') }}"></script>
	<script src="{{ url('/js/jquery.min.js') }}"></script>
@endsection
